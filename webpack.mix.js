const { mix } = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */
//==============
// VARIABLES
//==============
var publicPath = 'laravel/public/assets/';
var resourcesPath = 'laravel/resources/assets/';
var pluginsPath = 'laravel/public/assets/plugins/';

mix.setPublicPath(publicPath);

//==============
// JAVASCRIPT
//==============
mix.js(resourcesPath + 'js/app.js', 'js')

//==============
//STYLE SHEET
//==============
.sass(resourcesPath + 'sass/app.scss', 'css')

//==============
//COPY FILES
//==============

//js
.copy('node_modules/tinymce/tinymce.min.js', (publicPath + 'js/tinymce.min.js'))

.copy('node_modules/jszip/dist/jszip.js', (pluginsPath + 'datatables/js/jszip.js'))
.copy(resourcesPath + 'plugins/datatables/js/jquery.dataTables.js', (pluginsPath + 'datatables/js/jquery.dataTables.js'))
.copy('node_modules/datatables.net-bs/js/dataTables.bootstrap.js', (pluginsPath + 'datatables/js/dataTables.bootstrap.js'))
.copy('node_modules/datatables.net-buttons/js/dataTables.buttons.js', (pluginsPath + 'datatables/js/dataTables.buttons.js'))
.copy('node_modules/datatables.net-buttons/js/buttons.flash.js', (pluginsPath + 'datatables/js/buttons.flash.js'))
.copy('node_modules/datatables.net-buttons/js/buttons.html5.js', (pluginsPath + 'datatables/js/buttons.html5.js'))
.copy('node_modules/datatables.net-buttons/js/buttons.print.js', (pluginsPath + 'datatables/js/buttons.print.js'))
.copy('node_modules/datatables.net-buttons/js/buttons.colVis.js', (pluginsPath + 'datatables/js/buttons.colVis.js'))
.copy('node_modules/datatables.net-buttons-bs/js/buttons.bootstrap.js', (pluginsPath + 'datatables/js/buttons.bootstrap.js'))
.copy('node_modules/jquery-confirm/js/jquery-confirm.js', (pluginsPath + 'jquery-confirm/js/jquery-confirm.js'))

//css
.copy('node_modules/mdi/css/materialdesignicons.css', (pluginsPath + 'mdi/css/materialdesignicons.css'))
.copy('node_modules/datatables.net-bs/css/dataTables.bootstrap.css', (pluginsPath + 'datatables/css/dataTables.bootstrap.css'))
.copy('node_modules/jquery-confirm/css/jquery-confirm.css', (pluginsPath + 'jquery-confirm/css/jquery-confirm.css'))

//fonts
.copy('node_modules/mdi/fonts', (pluginsPath + 'mdi/fonts/'))

//==============
//COPY FOLDER
//==============
.copyDirectory('node_modules/tinymce/plugins', (publicPath + 'js/plugins'))
.copyDirectory('node_modules/tinymce/skins', (publicPath + 'js/skins'))
.copyDirectory('node_modules/tinymce/themes', (publicPath + 'js/themes'))
.copyDirectory(resourcesPath + 'plugins/jbox', (pluginsPath + 'jbox'));