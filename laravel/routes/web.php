<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['prefix' => 'admin', 'as' => 'admin.'], function () {
    Route::get('/', 'AdminHomeController@index');
    Route::get('dashboard', 'AdminHomeController@index')->name('dashboard');
    Route::get('filemanager', 'AdminHomeController@filemanager')->name('filemanager');

    Route::group(['prefix' => 'basics', 'as' => 'basics.'], function () {
        Route::group(['prefix' => 'languages', 'as' => 'languages.'], function () {
            Route::get('/', 'LanguageController@index');
            Route::get('index', 'LanguageController@index')->name('index');
            Route::get('create', 'LanguageController@create')->name('create');
            Route::get('edit/{id}', 'LanguageController@edit')->name('edit');
            Route::post('store', 'LanguageController@store')->name('store');
            Route::post('update/{id}', 'LanguageController@update')->name('update');
            Route::get('delete/{id}', 'LanguageController@delete')->name('delete');
        });

        Route::group(['prefix' => 'tables', 'as' => 'tables.'], function () {
            Route::get('/', 'TableController@index');
            Route::get('index', 'TableController@index')->name('index');
            Route::get('create', 'TableController@create')->name('create');
            Route::get('edit/{id}', 'TableController@edit')->name('edit');
            Route::post('store', 'TableController@store')->name('store');
            Route::post('update/{id}', 'TableController@update')->name('update');
            Route::get('delete/{id}', 'TableController@delete')->name('delete');
        });

        Route::group(['prefix' => 'fields', 'as' => 'fields.'], function () {
            Route::get('/', 'FieldController@index');
            Route::get('index', 'FieldController@index')->name('index');
            Route::get('create', 'FieldController@create')->name('create');
            Route::get('edit/{id}', 'FieldController@edit')->name('edit');
            Route::post('store', 'FieldController@store')->name('store');
            Route::post('update/{id}', 'FieldController@update')->name('update');
            Route::get('delete/{id}', 'FieldController@delete')->name('delete');
        });

        Route::group(['prefix' => 'tags', 'as' => 'tags.'], function () {
            Route::get('/', 'TagController@index');
            Route::get('index', 'TagController@index')->name('index');
            Route::get('create', 'TagController@create')->name('create');
            Route::get('edit/{id}', 'TagController@edit')->name('edit');
            Route::post('store', 'TagController@store')->name('store');
            Route::post('update/{id}', 'TagController@update')->name('update');
            Route::get('delete/{id}', 'TagController@delete')->name('delete');
        });

        Route::group(['prefix' => 'units', 'as' => 'units.'], function () {
            Route::get('/', 'UnitController@index');
            Route::get('index', 'UnitController@index')->name('index');
            Route::get('create', 'UnitController@create')->name('create');
            Route::get('edit/{id}', 'UnitController@edit')->name('edit');
            Route::post('store', 'UnitController@store')->name('store');
            Route::post('update/{id}', 'UnitController@update')->name('update');
            Route::get('delete/{id}', 'UnitController@delete')->name('delete');
        });
    });

    Route::group(['prefix' => 'pages', 'as' => 'pages.'], function () {
        Route::get('/', 'PageController@index');
        Route::get('index', 'PageController@index')->name('index');
        Route::get('create', 'PageController@create')->name('create');
        Route::get('edit/{id}', 'PageController@edit')->name('edit');
        Route::post('store', 'PageController@store')->name('store');
        Route::post('update/{id}', 'PageController@update')->name('update');
        Route::get('delete/{id}', 'PageController@delete')->name('delete');
    });

    Route::group(['prefix' => 'activities', 'as' => 'activities.'], function () {
        Route::get('/', 'ActivityController@index');
        Route::get('index', 'ActivityController@index')->name('index');
        Route::get('create', 'ActivityController@create')->name('create');
        Route::get('edit/{id}', 'ActivityController@edit')->name('edit');
        Route::post('store', 'ActivityController@store')->name('store');
        Route::post('update/{id}', 'ActivityController@update')->name('update');
        Route::get('delete/{id}', 'ActivityController@delete')->name('delete');
    });

    Route::group(['prefix' => 'foods', 'as' => 'foods.'], function () {
        Route::get('/', 'FoodController@index');
        Route::get('index', 'FoodController@index')->name('index');
        Route::get('create', 'FoodController@create')->name('create');
        Route::get('edit/{id}', 'FoodController@edit')->name('edit');
        Route::post('store', 'FoodController@store')->name('store');
        Route::post('update/{id}', 'FoodController@update')->name('update');
        Route::get('delete/{id}', 'FoodController@delete')->name('delete');

        Route::group(['prefix' => 'groups', 'as' => 'groups.'], function () {
            Route::get('/', 'FoodGroupController@index');
            Route::get('index', 'FoodGroupController@index')->name('index');
            Route::get('create', 'FoodGroupController@create')->name('create');
            Route::get('edit/{id}', 'FoodGroupController@edit')->name('edit');
            Route::post('store', 'FoodGroupController@store')->name('store');
            Route::post('update/{id}', 'FoodGroupController@update')->name('update');
            Route::get('delete/{id}', 'FoodGroupController@delete')->name('delete');
        });

        Route::group(['prefix' => 'sizes', 'as' => 'sizes.'], function () {
            Route::get('/', 'SizeController@index');
            Route::get('index', 'SizeController@index')->name('index');
            Route::get('create', 'SizeController@create')->name('create');
            Route::get('edit/{id}', 'SizeController@edit')->name('edit');
            Route::post('store', 'SizeController@store')->name('store');
            Route::post('update/{id}', 'SizeController@update')->name('update');
            Route::get('delete/{id}', 'SizeController@delete')->name('delete');
        });

        Route::group(['prefix' => 'facts', 'as' => 'facts.'], function () {
            Route::get('/', 'FactController@index');
            Route::get('index', 'FactController@index')->name('index');
            Route::get('create', 'FactController@create')->name('create');
            Route::get('edit/{id}', 'FactController@edit')->name('edit');
            Route::post('store', 'FactController@store')->name('store');
            Route::post('update/{id}', 'FactController@update')->name('update');
            Route::get('delete/{id}', 'FactController@delete')->name('delete');
        });
    });

    Route::group(['prefix' => 'users', 'as' => 'users.'], function () {
        Route::get('/', 'BiosibUserController@index');
        Route::get('index', 'BiosibUserController@index')->name('index');
        Route::get('create', 'BiosibUserController@create')->name('create');
        Route::get('edit/{id}', 'BiosibUserController@edit')->name('edit');
        Route::post('store', 'BiosibUserController@store')->name('store');
        Route::post('update/{id}', 'BiosibUserController@update')->name('update');
        Route::get('delete/{id}', 'BiosibUserController@delete')->name('delete');
    });

});


Route::group(['prefix' => 'users', 'as' => 'users.'], function () {
    Route::get('/register', 'BiosibUserController@show_register')->name('register');
    Route::post('/register', 'BiosibUserController@register')->name('register');
}); 