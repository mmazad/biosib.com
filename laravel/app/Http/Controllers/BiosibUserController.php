<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\BiosibUserRegisterRequest;
use Sentinel;

class BiosibUserController extends Controller
{
    public function index() {

        $users = BiosibUser::all();
             
        return view('pages.esmart_users.index')->with('users', $users);
    }

    public function show_register(Request $request)
    {
        if ($request->session()->has('registered_user')) {
            $request->session()->forget('registered_user');
            return view('pages.biosib_users.successful_registration');    
        }        
        return view('pages.biosib_users.register');                                                                                                    
    }

    /*
    * Response to user registration request
    */

    public function register(BiosibUserRegisterRequest $request)
    {
    	try {

	        $user = Sentinel::register([
	            'email' => $request->input('email'),
	            'username' => $request->input('username'),
	            'password' => $request->input('password'),
	            'first_name' => $request->input('first_name'),
	            'last_name' => $request->input('last_name'),
	            // 'national_code' => $request->input('national_code'),
	            // 'birth_year' =>  $request->input('birth_year'),
	            // 'birth_month' => $request->input('birth_month'),
	            // 'birth_day' => $request->input('birth_day'),
	            // 'company' => $request->input('company'),
	            // 'activity_field' => $request->input('activity_field'),
	            // 'country_id' => $country_id,
	            // 'province_id' => $province_id,
	            // 'city_id' => $city_id,
	            // 'tel' => $request->input('tel'),
	            // 'mobile' => $request->input('mobile'),
	            // 'address' => $request->input('address'),
	            // 'postal_code' => $request->input('postal_code'),
	            // 'status' => '0',
	        ]);

    		$request->session()->put('registered_user', 'true');
        	return redirect()->route('users.register_result')->with($result,'OK');	
    	} catch (Exception $e) {
    		return redirect()->route('users.register_result');		
    	}


        //$user->roles()->sync([3]);

    }

}
