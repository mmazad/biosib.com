<?php

namespace App\Http\Controllers;

use App\Http\Requests\FoodGroupRequest;
use App\Models\FoodGroup;
use App\Models\Table;
use App\Models\Field;
use App\Models\Translation;
use App\Models\Language;
use Illuminate\Http\Request;
use Mockery\CountValidator\Exception;
use Illuminate\Support\Facades\DB;

class FoodGroupController extends Controller
{
    public function index()
    {
        $foodgroups = FoodGroup::all();
        return view("pages.foodgroups.index")->with("foodgroups", $foodgroups);
    }

    public function delete(Request $request, $id)
    {
        if ($request->isMethod('get')) {
            DB::BeginTransaction();
            $foodgroup = FoodGroup::find($id);
            try {
                $foodgroup->translations()->delete();
                $foodgroup->delete();
                DB::commit();
                return redirect()->route('admin.foods.groups.index');
            } catch (Exception $e) {
                DB::rollback();
                return redirect()->route('admin.foods.groups.index');
            }
        }
    }

    public function create()
    {
        $languages = Language::all();
        return view("pages.foodgroups.create")->with('languages', $languages);
    }

    public function store(FoodGroupRequest $request)
    {
        if ($request->ajax()) {
            DB::BeginTransaction();
            try {
                $foodgroup = new FoodGroup;
                $foodgroup->is_active = $request->input('is_active') ? 1 : 0;
                $foodgroup->name = $request->input("name");
                $table = Table::where('name', 'food_groups')->first();
                $foodgroup->table_id = $table->id;
                $foodgroup->save();
                $languages = Language::all();
                $fields = $foodgroup->table->fields;
                if ($languages && $fields) {
                    foreach ($fields as $field) {
                        foreach ($languages as $language) {
                            if ($request->has($field->name . '-' . $language->id)) {
                                $translation = new Translation;
                                $translation->is_active = 1;
                                $translation->language_id = $language->id;
                                $translation->field_id = $field->id;
                                $translation->translation = $request->input($field->name . '-' . $language->id);
                                $foodgroup->translations()->save($translation);
                            }
                        }
                    }
                    DB::commit();
                } else
                    DB::rollback();
                return response()->json(['redirect_url' => route('admin.foods.groups.index')]);
            } catch (Exception $e) {
                DB::rollback();
                return response()->json(['redirect_url' => route('admin.foods.groups.create')]);
            }
        }
    }

    public function edit($id)
    {
        $foodgroup = FoodGroup::find($id);
        $languages = Language::all();
        return view("pages.foodgroups.edit")->with("foodgroup", $foodgroup)->with("languages", $languages);
    }

    public function update(FoodGroupRequest $request, $id)
    {
        if ($request->ajax()) {
            DB::BeginTransaction();
            $foodgroup = FoodGroup::find($id);
            try {
                $foodgroup->is_active = $request->input('is_active') ? 1 : 0;
                $foodgroup->name = $request->input("name");
                $foodgroup->save();
                $languages = Language::all();
                $fields = $foodgroup->table->fields;
                if ($languages && $fields) {
                    foreach ($fields as $field) {
                        foreach ($languages as $language) {
                            if ($request->has($field->name . '-' . $language->id)) {
                                $translation = $foodgroup->field_by_language($field->name, $language);
                                $translation->translation = $request->input($field->name . '-' . $language->id);
                                $translation->save();
                            }
                        }
                    }
                    DB::commit();
                } else
                    DB::rollback();
                return response()->json(['redirect_url' => route('admin.foods.groups.index')]);
            } catch (Exception $e) {
                DB::rollback();
                return response()->json(['redirect_url' => route('admin.foods.groups.index')]);
            }
        }
    }
}
