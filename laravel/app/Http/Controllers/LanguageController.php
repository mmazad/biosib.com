<?php

namespace App\Http\Controllers;

use App\Http\Requests\LanguageRequest;
use App\Models\Language;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class LanguageController extends Controller
{
    public function index()
    {
        $languages = Language::all();
        return view("pages.languages.index")->with("languages", $languages);
    }

    public function delete(Request $request, $id)
    {
        if ($request->isMethod('get')) {
            try {
                $language = Language::find($id);
                $language->delete();
                return redirect()->route('admin.basics.languages.index');
            } catch (Exception $e) {
                return redirect()->route('admin.basics.languages.index');
            }
        }
    }

    public function create()
    {
        return view("pages.languages.create");
    }

    public function store(LanguageRequest $request)
    {
        if ($request->ajax()) {
            DB::BeginTransaction();
            $language = new Language();
            try {
                $language->is_active = $request->input('is_active') ? 1 : 0;
                $language->dir = $request->input('dir');
                $language->name = $request->input("name");
                $language->abbr = $request->input("abbr");
                $language->native_name = $request->input("native_name");
                $language->save();
                DB::commit();
                return response()->json(['redirect_url' => route('admin.basics.languages.index')]);
            } catch (Exception $e) {
                DB::rollback();
                return response()->json(['redirect_url' => route('admin.basics.languages.create')]);
            }
        }
    }

    public function edit($id)
    {
        $language = Language::find($id);
        return view("pages.languages.edit")->with("language", $language);
    }

    public function update(LanguageRequest $request, $id)
    {
        if ($request->ajax()) {
            DB::BeginTransaction();
            $language = Language::find($id);
            try {
                $language->is_active = $request->input('is_active') ? 1 : 0;
                $language->dir = $request->input('dir');
                $language->name = $request->input("name");
                $language->abbr = $request->input("abbr");
                $language->native_name = $request->input("native_name");
                $language->save();
                DB::commit();
                return response()->json(['redirect_url' => route('admin.basics.languages.index')]);
            } catch (Exception $e) {
                DB::rollback();
                return response()->json(['redirect_url' => route('admin.basics.languages.index')]);
            }
        }
    }
}
