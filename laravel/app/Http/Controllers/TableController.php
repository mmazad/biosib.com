<?php

namespace App\Http\Controllers;

use App\Http\Requests\TableRequest;
use App\Models\Table;
use Illuminate\Http\Request;

class TableController extends Controller
{
    public function index()
    {
        $tables = Table::all();
        return view("pages.tables.index")->with("tables", $tables);
    }

    public function delete(Request $request, $id)
    {
        if ($request->isMethod('get')) {
            try {
                $table = Table::find($id);
                $table->delete();
                return redirect()->route('admin.basics.tables.index');
            } catch (Exception $e) {
                return redirect()->route('admin.basics.tables.index');
            }
        }
    }

    public function create()
    {
        return view("pages.tables.create");
    }

    public function store(TableRequest $request)
    {
        if ($request->ajax()) {
            $table = new Table();
            try {
                $table->name = $request->input("name");
                $table->save();
                return response()->json(['redirect_url' => route('admin.basics.tables.index')]);
            } catch (Exception $e) {
                return response()->json(['redirect_url' => route('admin.basics.tables.create')]);
            }
        }
    }

    public function edit($id)
    {
        $table = Table::find($id);
        return view("pages.tables.edit")->with("table", $table);
    }

    public function update(TableRequest $request, $id)
    {
        if ($request->ajax()) {
            $table = Table::find($id);
            try {
                $table->name = $request->input("name");
                $table->save();
                return response()->json(['redirect_url' => route('admin.basics.tables.index')]);
            } catch (Exception $e) {
                return response()->json(['redirect_url' => route('admin.basics.tables.index')]);
            }
        }
    }
}
