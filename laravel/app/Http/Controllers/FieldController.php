<?php

namespace App\Http\Controllers;

use App\Http\Requests\FieldRequest;
use App\Models\Field;
use App\Models\Table;
use Illuminate\Http\Request;

class FieldController extends Controller
{
    public function index()
    {
        $fields = Field::all();
        return view("pages.fields.index")->with("fields", $fields);
    }

    public function delete(Request $request, $id)
    {
        if ($request->isMethod('get')) {
            try {
                $field = Field::find($id);
                $field->delete();
                return redirect()->route('admin.basics.fields.index');
            } catch (Exception $e) {
                return redirect()->route('admin.basics.fields.index');
            }
        }
    }

    public function create()
    {
        $table_ids = Table::all()->pluck('name', 'id');
        return view("pages.fields.create")->with('table_ids', $table_ids);
    }

    public function store(FieldRequest $request)
    {
        if ($request->ajax()) {
            $field = new Field();
            try {
                $field->name = $request->input("name");
                $field->table_id = $request->input("table_id");
                $field->save();
                return response()->json(['redirect_url' => route('admin.basics.fields.index')]);
            } catch (Exception $e) {
                return response()->json(['redirect_url' => route('admin.basics.fields.create')]);
            }
        }
    }

    public function edit($id)
    {
        $field = Field::find($id);
        $table_ids = Table::all()->pluck('name', 'id');
        return view("pages.fields.edit")->with("field", $field)->with('table_ids', $table_ids);
    }

    public function update(FieldRequest $request, $id)
    {
        if ($request->ajax()) {
            $field = Field::find($id);
            try {
                $field->name = $request->input("name");
                $field->save();
                return response()->json(['redirect_url' => route('admin.basics.fields.index')]);
            } catch (Exception $e) {
                return response()->json(['redirect_url' => route('admin.basics.fields.index')]);
            }
        }
    }
}
