<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\TagRequest;
use App\Models\Tag;
use App\Models\Table;
use App\Models\Field;
use App\Models\Translation;
use App\Models\Language;
use Mockery\CountValidator\Exception;
use Illuminate\Support\Facades\DB;

class TagController extends Controller
{
    public function index()
    {
        $tags = Tag::all();
        return view("pages.tags.index")->with("tags", $tags);
    }

    public function delete(Request $request, $id)
    {
        if ($request->isMethod('get')) {
            DB::BeginTransaction();
            $tag = Tag::find($id);
            try {
                $tag->translations()->delete();
                $tag->delete();
                DB::commit();
                return redirect()->route('admin.basics.tags.index');
            } catch (Exception $e) {
                DB::rollback();
                return redirect()->route('admin.basics.tags.index');
            }
        }
    }

    public function create()
    {
        $languages = Language::all();
        return view("pages.tags.create")->with('languages', $languages);
    }

    public function store(TagRequest $request)
    {
        if ($request->ajax()) {
            DB::BeginTransaction();
            try {
                $tag = new tag;
                $tag->is_active = $request->input('is_active') ? 1 : 0;
                $tag->name = $request->input("name");
                $table = Table::where('name', 'tags')->first();
                $tag->table_id = $table->id;
                $tag->save();
                $languages = Language::all();
                $fields = $tag->table->fields;
                if ($languages && $fields) {
                    foreach ($fields as $field) {
                        foreach ($languages as $language) {
                            if ($request->has($field->name . '-' . $language->id)) {
                                $translation = new Translation;
                                $translation->is_active = 1;
                                $translation->language_id = $language->id;
                                $translation->field_id = $field->id;
                                $translation->translation = $request->input($field->name . '-' . $language->id);
                                $tag->translations()->save($translation);
                            }
                        }
                    }
                    DB::commit();
                } else
                    DB::rollback();
                return response()->json(['redirect_url' => route('admin.basics.tags.index')]);
            } catch (Exception $e) {
                DB::rollback();
                return response()->json(['redirect_url' => route('admin.basics.tags.create')]);
            }
        }
    }

    public function edit($id)
    {
        $tag = Tag::find($id);
        $languages = Language::all();
        return view("pages.tags.edit")->with("tag", $tag)->with("languages", $languages);
    }

    public function update(TagRequest $request, $id)
    {
        if ($request->ajax()) {
            DB::BeginTransaction();
            $tag = Tag::find($id);
            try {
                $tag->is_active = $request->input('is_active') ? 1 : 0;
                $tag->name = $request->input("name");
                $tag->save();
                $languages = Language::all();
                $fields = $tag->table->fields;
                if ($languages && $fields) {
                    foreach ($fields as $field) {
                        foreach ($languages as $language) {
                            if ($request->has($field->name . '-' . $language->id)) {
                                $translation = $tag->field_by_language($field->name, $language);
                                $translation->translation = $request->input($field->name . '-' . $language->id);
                                $translation->save();
                            }
                        }
                    }
                    DB::commit();
                } else
                    DB::rollback();
                return response()->json(['redirect_url' => route('admin.basics.tags.index')]);
            } catch (Exception $e) {
                DB::rollback();
                return response()->json(['redirect_url' => route('admin.basics.tags.index')]);
            }
        }
    }
}
