<?php

namespace App\Http\Controllers;

use App\Http\Requests\FoodRequest;
use App\Models\Food;
use App\Models\FoodGroup;
use App\Models\Language;
use App\Models\Page;
use App\Models\Size;
use App\Models\Tag;
use Illuminate\Http\Request;

class FoodController extends Controller
{
    public function index()
    {
        $foods = Food::all();
        return view("pages.foods.index")->with("foods", $foods);
    }

    public function delete(Request $request, $id)
    {
        if ($request->isMethod('get')) {
            DB::BeginTransaction();
            $foods = Food::find($id);
            try {
                $foods->translations()->delete();
                $foods->delete();
                DB::commit();
                return redirect()->route('admin.foods.index');
            } catch (Exception $e) {
                DB::rollback();
                return redirect()->route('admin.foods.index');
            }
        }
    }

    public function create()
    {
        $languages = Language::all();
        $tag_ids = Tag::bio_pluck('title', 'fa');
        $size_ids = Size::bio_pluck('title', 'fa');
        $page_ids = [];//Page::bio_pluck('title', 'fa');
        $food_group_ids = FoodGroup::bio_pluck('title', 'fa');
        return view("pages.foods.create")
            ->with('languages', $languages)
            ->with('food_group_ids', $food_group_ids)
            ->with('tag_ids', $tag_ids)
            ->with("page_ids", $page_ids)
            ->with("size_ids", $size_ids);
    }

    public function store(FoodRequest $request)
    {
//        if ($request->ajax()) {
//            DB::BeginTransaction();
//            try {
//                $food = new Food;
//                $food->is_active = $request->input('is_active') ? 1 : 0;
//                $food->name = $request->input("name");
//                $food->images = $request->input('food_images');
//                $table = Table::where('name', 'foods')->first();
//                $food->table_id = $table->id;
//                $food->save();
//
//                $tags = $request->input('tag_id');
//                foreach ($tags as $tag)
//                {
//                    $new_tag = new Tag;
//                    $new_tag
//                }
//                $food->tags()
 //
 //               $languages = Language::all();
 //               $fields = $food->table->fields;
 //               if ($languages && $fields) {
 //                   foreach ($fields as $field) {
 //                       foreach ($languages as $language) {
 //                           if ($request->has($field->name . '-' . $language->id)) {
 //                               $translation = new Translation;
 //                               $translation->is_active = 1;
 //                               $translation->language_id = $language->id;
 //                               $translation->field_id = $field->id;
 //                               $translation->translation = $request->input($field->name . '-' . $language->id);
 //                               $tag->translations()->save($translation);
 //                           }
 //                       }
 //                   }
 //                   DB::commit();
 //               } else
 //                   DB::rollback();
 //               return response()->json(['redirect_url' => route('admin.basics.tags.index')]);
 //           } catch (Exception $e) {
 //               DB::rollback();
 //               return response()->json(['redirect_url' => route('admin.basics.tags.create')]);
 //           }
//        }
    }
}
