<?php

namespace App\Http\Controllers;

use App\Models\Unit;
use Illuminate\Http\Request;
use App\Http\Requests\FactRequest;
use App\Models\Fact;
use App\Models\Table;
use App\Models\Translation;
use App\Models\Language;
use Mockery\CountValidator\Exception;
use Illuminate\Support\Facades\DB;

class FactController extends Controller
{
    public function index()
    {
        $facts = Fact::all();
        return view("pages.facts.index")->with("facts", $facts);
    }

    public function delete(Request $request, $id)
    {
        if ($request->isMethod('get')) {
            DB::BeginTransaction();
            $fact = Fact::find($id);
            try {
                $fact->translations()->delete();
                $fact->delete();
                DB::commit();
                return redirect()->route('admin.foods.facts.index');
            } catch (Exception $e) {
                DB::rollback();
                return redirect()->route('admin.foods.facts.index');
            }
        }
    }

    public function create()
    {
        $languages = Language::all();
        $unit_ids = Unit::bio_pluck('title', 'fa');
        return view("pages.facts.create")->with('languages', $languages)->with('unit_ids', $unit_ids);
    }

    public function store(FactRequest $request)
    {
        if ($request->ajax()) {
            DB::BeginTransaction();
            try {
                $fact = new fact;
                $fact->is_active = $request->input('is_active') ? 1 : 0;
                $fact->name = $request->input("name");
                $fact->unit_id = $request->input("unit_id");
                $table = Table::where('name', 'sizes')->first();
                $fact->table_id = $table->id;
                $fact->save();
                $languages = Language::all();
                $fields = $fact->table->fields;
                if ($languages && $fields) {
                    foreach ($fields as $field) {
                        foreach ($languages as $language) {
                            if ($request->has($field->name . '-' . $language->id)) {
                                $translation = new Translation;
                                $translation->is_active = 1;
                                $translation->language_id = $language->id;
                                $translation->field_id = $field->id;
                                $translation->translation = $request->input($field->name . '-' . $language->id);
                                $fact->translations()->save($translation);
                            }
                        }
                    }
                    DB::commit();
                } else
                    DB::rollback();
                return response()->json(['redirect_url' => route('admin.foods.facts.index')]);
            } catch (Exception $e) {
                DB::rollback();
                return response()->json(['redirect_url' => route('admin.foods.facts.create')]);
            }
        }
    }

    public function edit($id)
    {
        $fact = Fact::find($id);
        $unit_ids = Unit::bio_pluck('title', 'fa');
        $languages = Language::all();
        return view("pages.facts.edit")->with("fact", $fact)->with("languages", $languages)->with("unit_ids", $unit_ids);
    }

    public function update(FactRequest $request, $id)
    {
        if ($request->ajax()) {
            DB::BeginTransaction();
            try {
                $fact = Fact::find($id);
                $fact->is_active = $request->input('is_active') ? 1 : 0;
                $fact->name = $request->input("name");
                $fact->unit_id = $request->input("unit_id");
                $table = Table::where('name', 'sizes')->first();
                $fact->table_id = $table->id;
                $fact->save();
                $languages = Language::all();
                $fields = $fact->table->fields;
                if ($languages && $fields) {
                    foreach ($fields as $field) {
                        foreach ($languages as $language) {
                            if ($request->has($field->name . '-' . $language->id)) {
                                $translation = $fact->field_by_language($field->name, $language);
                                $translation->translation = $request->input($field->name . '-' . $language->id);
                                $translation->save();
                            }
                        }
                    }
                    DB::commit();
                } else
                    DB::rollback();
                return response()->json(['redirect_url' => route('admin.foods.facts.index')]);
            } catch (Exception $e) {
                DB::rollback();
                return response()->json(['redirect_url' => route('admin.foods.facts.index')]);
            }
        }
    }
}
