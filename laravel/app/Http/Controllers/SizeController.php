<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\SizeRequest;
use App\Models\Size;
use App\Models\Table;
use App\Models\Translation;
use App\Models\Language;
use Mockery\CountValidator\Exception;
use Illuminate\Support\Facades\DB;

class SizeController extends Controller
{
    public function index()
    {
        $sizes = Size::all();
        return view("pages.sizes.index")->with("sizes", $sizes);
    }

    public function delete(Request $request, $id)
    {
        if ($request->isMethod('get')) {
            DB::BeginTransaction();
            $size = Size::find($id);
            try {
                $size->translations()->delete();
                $size->delete();
                DB::commit();
                return redirect()->route('admin.foods.sizes.index');
            } catch (Exception $e) {
                DB::rollback();
                return redirect()->route('admin.foods.sizes.index');
            }
        }
    }

    public function create()
    {
        $languages = Language::all();
        return view("pages.sizes.create")->with('languages', $languages);
    }

    public function store(SizeRequest $request)
    {
        if ($request->ajax()) {
            DB::BeginTransaction();
            try {
                $size = new size;
                $size->is_active = $request->input('is_active') ? 1 : 0;
                $size->name = $request->input("name");
                $table = Table::where('name', 'sizes')->first();
                $size->table_id = $table->id;
                $size->save();
                $languages = Language::all();
                $fields = $size->table->fields;
                if ($languages && $fields) {
                    foreach ($fields as $field) {
                        foreach ($languages as $language) {
                            if ($request->has($field->name . '-' . $language->id)) {
                                $translation = new Translation;
                                $translation->is_active = 1;
                                $translation->language_id = $language->id;
                                $translation->field_id = $field->id;
                                $translation->translation = $request->input($field->name . '-' . $language->id);
                                $size->translations()->save($translation);
                            }
                        }
                    }
                    DB::commit();
                } else
                    DB::rollback();
                return response()->json(['redirect_url' => route('admin.foods.sizes.index')]);
            } catch (Exception $e) {
                DB::rollback();
                return response()->json(['redirect_url' => route('admin.foods.sizes.create')]);
            }
        }
    }

    public function edit($id)
    {
        $size = Size::find($id);
        $languages = Language::all();
        return view("pages.sizes.edit")->with("size", $size)->with("languages", $languages);
    }

    public function update(SizeRequest $request, $id)
    {
        if ($request->ajax()) {
            DB::BeginTransaction();
            $size = Size::find($id);
            try {
                $size->is_active = $request->input('is_active') ? 1 : 0;
                $size->name = $request->input("name");
                $table = Table::where('name', 'sizes')->first();
                $size->table_id = $table->id;
                $size->save();
                $languages = Language::all();
                $fields = $size->table->fields;
                if ($languages && $fields) {
                    foreach ($fields as $field) {
                        foreach ($languages as $language) {
                            if ($request->has($field->name . '-' . $language->id)) {
                                $translation = $size->field_by_language($field->name, $language);
                                $translation->translation = $request->input($field->name . '-' . $language->id);
                                $translation->save();
                            }
                        }
                    }
                    DB::commit();
                } else
                    DB::rollback();
                return response()->json(['redirect_url' => route('admin.foods.sizes.index')]);
            } catch (Exception $e) {
                DB::rollback();
                return response()->json(['redirect_url' => route('admin.foods.sizes.index')]);
            }
        }
    }
}
