<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\UnitRequest;
use App\Models\Unit;
use App\Models\Table;
use App\Models\Translation;
use App\Models\Language;
use Mockery\CountValidator\Exception;
use Illuminate\Support\Facades\DB;

class UnitController extends Controller
{
    public function index()
    {
        $units = Unit::all();
        return view("pages.units.index")->with("units", $units);
    }

    public function delete(Request $request, $id)
    {
        if ($request->isMethod('get')) {
            DB::BeginTransaction();
            $unit = Unit::find($id);
            try {
                $unit->translations()->delete();
                $unit->delete();
                DB::commit();
                return redirect()->route('admin.basics.units.index');
            } catch (Exception $e) {
                DB::rollback();
                return redirect()->route('admin.basics.units.index');
            }
        }
    }

    public function create()
    {
        $languages = Language::all();
        return view("pages.units.create")->with('languages', $languages);
    }

    public function store(UnitRequest $request)
    {
        if ($request->ajax()) {
            DB::BeginTransaction();
            try {
                $unit = new unit;
                $unit->is_active = $request->input('is_active') ? 1 : 0;
                $unit->name = $request->input("name");
                $table = Table::where('name', 'units')->first();
                $unit->table_id = $table->id;
                $unit->save();
                $languages = Language::all();
                $fields = $unit->table->fields;
                if ($languages && $fields) {
                    foreach ($fields as $field) {
                        foreach ($languages as $language) {
                            if ($request->has($field->name . '-' . $language->id)) {
                                $translation = new Translation;
                                $translation->is_active = 1;
                                $translation->language_id = $language->id;
                                $translation->field_id = $field->id;
                                $translation->translation = $request->input($field->name . '-' . $language->id);
                                $unit->translations()->save($translation);
                            }
                        }
                    }
                    DB::commit();
                } else
                    DB::rollback();
                return response()->json(['redirect_url' => route('admin.basics.units.index')]);
            } catch (Exception $e) {
                DB::rollback();
                return response()->json(['redirect_url' => route('admin.basics.units.create')]);
            }
        }
    }

    public function edit($id)
    {
        $unit = Unit::find($id);
        $languages = Language::all();
        return view("pages.units.edit")->with("unit", $unit)->with("languages", $languages);
    }

    public function update(UnitRequest $request, $id)
    {
        if ($request->ajax()) {
            DB::BeginTransaction();
            $unit = Unit::find($id);
            try {
                $unit->is_active = $request->input('is_active') ? 1 : 0;
                $unit->name = $request->input("name");
                $unit->save();
                $languages = Language::all();
                $fields = $unit->table->fields;
                if ($languages && $fields) {
                    foreach ($fields as $field) {
                        foreach ($languages as $language) {
                            if ($request->has($field->name . '-' . $language->id)) {
                                $translation = $unit->field_by_language($field->name, $language);
                                $translation->translation = $request->input($field->name . '-' . $language->id);
                                $translation->save();
                            }
                        }
                    }
                    DB::commit();
                } else
                    DB::rollback();
                return response()->json(['redirect_url' => route('admin.basics.units.index')]);
            } catch (Exception $e) {
                DB::rollback();
                return response()->json(['redirect_url' => route('admin.basics.units.index')]);
            }
        }
    }
}
