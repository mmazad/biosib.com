<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Models\Fact;
use App\Models\Language;

class FactRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $languages = Language::all();
        $rules = array();
        foreach ($languages as $language) {
            $rules['title-'.$language->id] = 'required';
        }
        switch ($this->action)
        {
            case "store":
                $rules['unit_id'] = 'required';
                $rules['name'] = 'required|max:50|regex:/^[a-zA-Z0-9 _-]*$/i|unique:facts,name';
                return $rules;
            case "update":
                $fact = Fact::find($this->id);
                $rules['unit_id'] = 'required';
                $rules['name'] = 'required|max:50|regex:/^[a-zA-Z0-9 _-]*$/i|unique:facts,name,' . $fact->id . ',id';
                return $rules;
            default:
                return [
                    "action_is_not_correct" => 'required'
                ];
        }
    }
}
