<?php

namespace App\Http\Requests;

use App\Models\Language;
use Illuminate\Foundation\Http\FormRequest;

class LanguageRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->action)
        {
            case 'store':
                return [
                    'name' => 'required|max:50|regex:/^[a-zA-Z0-9 _-]*$/i|unique:languages,name',
                    'abbr' => 'required|max:4|unique:languages,abbr',
                    'native_name' => 'required|max:50|unique:languages,native_name'
                ];
            case 'update':
                $language = Language::find($this->id);
                return [
                    'name' => 'required|max:50|regex:/^[a-zA-Z0-9 _-]*$/i|unique:languages,name,' . $language->id . ',id',
                    'abbr' => 'required|max:100|unique:languages,abbr,' . $language->id . ',id',
                    'native_name' => 'required|max:100|unique:languages,native_name,' . $language->id . ',id',
                ];
            default:
                return [
                    'action_is_not_correct' => 'required'
                ];
        }
    }
}
