<?php

namespace App\Http\Requests;

use App\Models\Table;
use Illuminate\Foundation\Http\FormRequest;

class TableRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->action)
        {
            case 'store':
                return [
                    'name' => 'required|max:50|regex:/^[a-zA-Z0-9 _-]*$/i|unique:tables,name',
                ];
            case 'update':
                $table = Table::find($this->id);
                return [
                    'name' => 'required|max:50|regex:/^[a-zA-Z0-9 _-]*$/i|unique:tables,name,' . $table->id . ',id',
                ];
            default:
                return [
                    'action_is_not_correct' => 'required'
                ];
        }
    }
}
