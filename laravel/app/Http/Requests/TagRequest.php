<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Models\Tag;
use App\Models\Language;

class TagRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $languages = Language::all();
        $rules = array();
        foreach ($languages as $language) {
            $rules['title-'.$language->id] = 'required';
        }        
        switch ($this->action)
        {
            case "store":
                $rules['name'] = 'required|max:50|regex:/^[a-zA-Z0-9 _-]*$/i|unique:tags,name';
                return $rules;
            case "update":
                $tags = Tag::find($this->id);
                $rules['name'] = 'required|max:50|regex:/^[a-zA-Z0-9 _-]*$/i|unique:tags,name,' . $tags->id . ',id';
                return $rules;                
            default:
                return [
                    "action_is_not_correct" => 'required'
                ];
        }
    }
}
