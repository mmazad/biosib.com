<?php

namespace App\Http\Requests;

use App\Models\Field;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class FieldRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->action) {
            case 'store':
                return [
                    'table_id' => 'required',
                    'name' => [
                        'required',
                        'max:50',
                        'regex:/^[a-zA-Z0-9 _-]*$/i',
                        Rule::unique('fields')->where(function ($query) {
                            $query->where('table_id', $this->table_id);
                        })
                    ]
                ];
            case 'update':
                $field = Field::find($this->id);
                return [
                    'table_id' => 'required',
                    'name' => 'required|max:50|regex:/^[a-zA-Z0-9 _-]*$/i|unique:fields,name,' . $field->id . ',id,table_id,' . $field->table_id,
                ];
            default:
                return [
                    'action_is_not_correct' => 'required'
                ];
        }
    }
}
