<?php

namespace App\Http\Requests;

use App\Models\FoodGroup;
use App\Models\Language;
use Illuminate\Foundation\Http\FormRequest;

class FoodGroupRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $languages = Language::all();
        $rules = array();
        foreach ($languages as $language) {
            $rules['title-'.$language->id] = 'required';
        }        
        switch ($this->action)
        {
            case 'store':
                $rules['name'] = 'required|max:50|regex:/^[a-zA-Z0-9 _-]*$/i|unique:food_groups,name';
                return $rules;
            case 'update':
                $foodgroup = FoodGroup::find($this->id);
                $rules['name'] = 'required|max:50|regex:/^[a-zA-Z0-9 _-]*$/i|unique:food_groups,name,' . $foodgroup->id . ',id';
                return $rules;                
            default:
                return [
                    'action_is_not_correct' => 'required'
                ];
        }
    }
}
