<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BiosibUserRegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'last_name'     => 'alpha_spaces|required',
            'first_name'    => 'alpha_spaces|required',          
        ];


        switch($this->method())
        {
            case 'GET':
            case 'DELETE':
            {
                break;
            }
            case 'POST':
            {
                $rules = array_merge($rules, array(
                    'email'                     => 'required|email|unique:users,email',
                    'password'  => 'required|min:6|confirmed',
                    ));
                break;
            }
            case 'PUT':
            case 'PATCH':
            {
                $user = BiosibUser::find($this->id);
                if ($this->input('password')) {
                $rules = array_merge($rules,array(
                    'password'  => 'required|min:6|confirmed',
                    ));
                }                 
                $rules = array_merge($rules,array(
                    'email'  => 'required|email|unique:users,email,'.$user->id.',id',
                    ));
                break;
            }
            default:break;
        }

        return $rules;
    }
}
