<?php

namespace App\Models;

use App;
use Illuminate\Database\Eloquent\Model;

class TransModel extends Model
{
    public function table(){
        return $this->belongsTo('App\Models\Table');
    }

    public function translations()
    {
        return $this->morphMany('App\Models\Translation', 'translatable');
    }

    public function translation($language)
    {
        return $this->morphMany('App\Models\Translation', 'translatable')->where('language_id', $language->id)->get();
    }

    public function field($field_name)
    {
        $locale = App::getLocale();
        $language = Language::where('abbr', $locale)->first();
        $field = $this->table()->first()->field($field_name);
        return $this->translation($language)->where('field_id', $field->id)->first();
    }

    public function field_by_language($field_name, $language) {
        $field = $this->table()->first()->field($field_name);
        return $this->translation($language)->where('field_id', $field->id)->first();
    }

    public static function bio_pluck($field_name, $abbr)
    {
        $language = Language::where('abbr', $abbr)->first();
        $list = [];
        $models = self::all();
        foreach ($models as $model)
            $list[$model->id] = $model->field_by_language($field_name, $language)->translation;
        return $list;
    }
}
