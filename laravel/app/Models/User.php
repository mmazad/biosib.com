<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    public $timestamps = false;

    public function foods()
    {
    	return $this->belongsToMany('App\Model\Food');
    }
}
