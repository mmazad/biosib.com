<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class Fact extends TransModel
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];

    public function unit()
    {
    	return $this->belongsTo('App\Model\Unit');
    }
}
