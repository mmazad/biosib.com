<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    public $timestamps = false;

    public function tags()
    {
		return $this->morphToMany('App\Models\Tag', 'taggable');
    }
}

