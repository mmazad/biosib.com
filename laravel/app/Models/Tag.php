<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class Tag extends TransModel
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];

    public function foods()
    {
        return $this->morphedByMany('App\Models\Food', 'taggable');
    }

    public function pages()
    {
        return $this->morphedByMany('App\Models\Page', 'taggable');
    }

}
