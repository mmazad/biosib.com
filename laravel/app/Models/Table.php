<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class Table extends TransModel
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];

    public function fields()
    {
        return $this->hasMany('App\Models\Field');
    }

    public function field($field_name)
    {
        return $this->fields()->where('name', $field_name)->first();
    }
}
