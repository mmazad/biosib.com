<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class Size extends TransModel
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];

    public function foods()
    {
    	return $this->belongsToMany('App\Model\Food');
    }
}
