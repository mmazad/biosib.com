<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class Field extends TransModel
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
}
