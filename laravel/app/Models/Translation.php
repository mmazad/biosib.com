<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Translation extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];

    public function translatable()
    {
        return $this->morphTo();
    }

    public function field()
    {
        return $this->belongsTo('App\Models\Field');
    }    

    public function language()
    {
        return $this->belongsTo('App\Models\Language');
    }    

}
