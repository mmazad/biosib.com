<?php

namespace App\Models;

use Cartalyst\Sentinel\Users\EloquentUser as SentinelUser;

class BiosibUser extends SentinelUser {

    // protected $fillable = [
    //     'email',
    //     'password',
    //     'username',
    //     'last_name',
    //     'first_name',
    //     'permissions',
    //     'national_code',
    //     'birth_year',
    //     'birth_month',
    //     'birth_day',
    //     'company',
    //     'country_id',
    //     'province_id',
    //     'city_id',
    //     'activity_field',
    //     'tel',
    //     'mobile',
    //     'address',
    //     'postal_code',
    //     'status',
    // ];

    protected $loginNames = ['email', 'username'];

    public function foods() {
        return $this->belongsToMany('App\Models\Food');
    }    

}

