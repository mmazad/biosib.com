<?php

namespace App\Models;
use Illuminate\Database\Eloquent\SoftDeletes;

class FoodGroup extends TransModel
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];

    public function foods(){
    	return $this->hasMany('App\Models\Food');
    }
}
