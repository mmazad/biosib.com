<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class Unit extends TransModel
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];

    public function facts()
    {
        return $this->hasMany('App\Model\Fact');
    }
}
