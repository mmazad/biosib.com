<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Food extends Model
{
    public $timestamps = false;

    public function group()
    {
    	return $this->belongsTo('App\Models\FoodGroup');
    }    

    public function tags()
    {
        return $this->morphToMany('App\Models\Tag', 'taggable');
    }

    public function userClicks()
    {
    	return $this->belongsToMany('App\Models\User');
    }

    public function size()
    {
        return $this->belongsToMany('App\Models\Size');
    }
}
