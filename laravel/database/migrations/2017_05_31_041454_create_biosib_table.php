<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBiosibTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('languages',function(Blueprint $table)
        {
            $table->increments('id');
            $table->boolean('is_active');
            $table->string('name',50)->unique();
            $table->string('native_name',50)->unique();
            $table->string('abbr',4)->unique();
            $table->integer('dir')->unsigned();
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('tables',function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('name',50)->unique();
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('fields',function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('table_id')->unsigned();
            $table->foreign('table_id')->references('id')->on('tables')->onUpdate('cascade')->onDelete('cascade');           
            $table->string('name',50);
            $table->unique(['table_id', 'name']);
            $table->timestamps();
            $table->softDeletes();            
        });        

        Schema::create('translations',function(Blueprint $table)
        {
            $table->increments('id');
            $table->boolean('is_active');
            $table->integer('language_id')->unsigned();
            $table->foreign('language_id')->references('id')->on('languages')->onUpdate('cascade')->onDelete('cascade');
            $table->integer('field_id')->unsigned();
            $table->foreign('field_id')->references('id')->on('fields')->onUpdate('cascade')->onDelete('cascade');             
            $table->morphs('translatable');
            $table->text('translation');
            $table->timestamps();
            $table->softDeletes();            
        });

        Schema::create('food_groups',function(Blueprint $table)
        {
            $table->increments('id');
            $table->boolean('is_active');
            $table->string('name')->unique();
            $table->integer('table_id')->unsigned();
            $table->foreign('table_id')->references('id')->on('tables')->onUpdate('cascade')->onDelete('cascade');
            $table->timestamps();
            $table->softDeletes();                                    
        });
       
        Schema::create('tags',function(Blueprint $table)
        {
            $table->increments('id');
            $table->boolean('is_active');
            $table->integer('parent_id')->unsigned()->nullable();
            $table->foreign('parent_id')->references('id')->on('tags')->onUpdate('cascade')->onDelete('set null');
            $table->string('name')->unique();
            $table->integer('table_id')->unsigned();
            $table->foreign('table_id')->references('id')->on('tables')->onUpdate('cascade')->onDelete('cascade');
            $table->timestamps();
            $table->softDeletes();                        
        });
       
        Schema::create('units', function (Blueprint $table)
        {
            $table->increments('id');
            $table->boolean('is_active');
            $table->string('name')->unique();
            $table->integer('table_id')->unsigned();
            $table->foreign('table_id')->references('id')->on('tables')->onUpdate('cascade')->onDelete('cascade');
            $table->timestamps();
            $table->softDeletes();                         
        });
        
        Schema::create('activities',function(Blueprint $table)
        {
            $table->increments('id');
            $table->boolean('is_active');
            $table->integer('calorie')->unsigned()->nullable();
            $table->string('name')->unique();
            $table->integer('table_id')->unsigned();
            $table->foreign('table_id')->references('id')->on('tables')->onUpdate('cascade')->onDelete('cascade'); 
            $table->timestamps();
            $table->softDeletes();                       
        });
       
        Schema::create('sizes',function(Blueprint $table)
        {
            $table->increments('id');
            $table->boolean('is_active');
            $table->string('name')->unique();
            $table->integer('table_id')->unsigned();
            $table->foreign('table_id')->references('id')->on('tables')->onUpdate('cascade')->onDelete('cascade');   
            $table->timestamps();
            $table->softDeletes();                     
        });

        Schema::create('pages', function (Blueprint $table) 
        {
            $table->increments('id');
            $table->integer('priority')->unsigned();
            $table->boolean('is_menu');
            $table->boolean('is_active');
            $table->string('name', 100)->unique();
            $table->string('folder_name', 100);
            $table->integer('parent_id')->unsigned()->nullable();
            $table->foreign('parent_id')->references('id')->on('pages')->onUpdate('cascade')->onDelete('set null');
            $table->integer('table_id')->unsigned();
            $table->foreign('table_id')->references('id')->on('tables')->onUpdate('cascade')->onDelete('cascade');
            $table->timestamps();
            $table->softDeletes();                                    
        });

        Schema::create('foods', function (Blueprint $table) 
        {
            $table->increments('id');
            $table->boolean('is_active');
            $table->integer('food_group_id')->unsigned()->nullable();
            $table->foreign('food_group_id')->references('id')->on('food_groups')->onUpdate('cascade')->onDelete('set null');
            $table->string('name')->unique();
            $table->json('images');
            $table->integer('table_id')->unsigned();
            $table->foreign('table_id')->references('id')->on('tables')->onUpdate('cascade')->onDelete('cascade');
            $table->timestamps();
            $table->softDeletes();                        
        });
        
        Schema::create('food_size', function (Blueprint $table)
        {
            $table->integer('food_id')->unsigned()->nullable();
            $table->foreign('food_id')->references('id')->on('foods')->onUpdate('cascade')->onDelete('set null');
            $table->integer('size_id')->unsigned()->nullable();
            $table->foreign('size_id')->references('id')->on('sizes')->onUpdate('cascade')->onDelete('set null');
            $table->integer('gram')->unsigned()->nullable();  
            $table->timestamps();
            $table->softDeletes();                                  
        });

        Schema::create('facts',function(Blueprint $table)
        {
            $table->increments('id');
            $table->boolean('is_active');
            $table->integer('unit_id')->unsigned()->nullable();
            $table->foreign('unit_id')->references('id')->on('units')->onUpdate('cascade')->onDelete('set null');
            $table->string('name')->unique();
            $table->integer('table_id')->unsigned();
            $table->foreign('table_id')->references('id')->on('tables')->onUpdate('cascade')->onDelete('cascade');
            $table->timestamps();
            $table->softDeletes();            
        });

        Schema::create('food_fact',function(Blueprint $table)
        {
            $table->integer('food_id')->unsigned();
            $table->foreign('food_id')->references('id')->on('foods')->onUpdate('cascade')->onDelete('cascade');
            $table->integer('fact_id')->unsigned();
            $table->foreign('fact_id')->references('id')->on('facts')->onUpdate('cascade')->onDelete('cascade');
            $table->integer('per_gram')->unsigned();
            $table->timestamps();
            $table->softDeletes();            
        });

        Schema::create('food_user',function(Blueprint $table)
        {
            $table->integer('user_id')->unsigned()->nullable();
            $table->foreign('user_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
            $table->integer('food_id')->unsigned()->nullable();
            $table->foreign('food_id')->references('id')->on('foods')->onUpdate('cascade')->onDelete('cascade');
            $table->integer('clicks')->unsigned();
            $table->timestamps();
            $table->softDeletes();            
        });

        Schema::create('likes', function (Blueprint $table)
        {
            $table->increments('id');
            $table->morphs('likeable');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
            $table->timestamps();
            $table->softDeletes();            
        });

        Schema::create('rates', function (Blueprint $table)
        {
            $table->increments('id');
            $table->morphs('rateable');   
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
            $table->integer('rate');
            $table->timestamps();
            $table->softDeletes();            
        });

        Schema::create('taggables', function (Blueprint $table)
        {
            $table->integer('tag_id')->unsigned();
            $table->foreign('tag_id')->references('id')->on('tags')->onUpdate('cascade')->onDelete('cascade');
            $table->morphs('taggables');
            $table->timestamps();
            $table->softDeletes();            
        });
        

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();

        Schema::dropIfExists('food_size');
        Schema::dropIfExists('food_user');
        Schema::dropIfExists('food_fact');
        Schema::dropIfExists('foods');
        Schema::dropIfExists('sizes');
        Schema::dropIfExists('food_groups');
        Schema::dropIfExists('units');
        Schema::dropIfExists('facts');
        Schema::dropIfExists('activities');
        Schema::dropIfExists('tags');
        Schema::dropIfExists('pages');
        Schema::dropIfExists('translations');
        Schema::dropIfExists('fields');
        Schema::dropIfExists('tables');
        Schema::dropIfExists('languages');
        Schema::dropIfExists('likes');
        Schema::dropIfExists('rates');
        Schema::dropIfExists('taggables');

        Schema::enableForeignKeyConstraints();
    }
}

