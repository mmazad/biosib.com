<?php

use Illuminate\Database\Seeder;
use App\Models\Table;
use App\Models\Field;

class TablesAndFieldsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('fields')->delete();
        DB::table('tables')->delete();

        /****************************
        *   food_groups seeder  
        */

        $table = new Table;
        $table->name = 'food_groups';
        $table->save();        

        $field = new Field;
        $field->name = 'title';

        $table->fields()->save($field);

        /**/

        /****************************
        *   sizes seeder  
        */

        $table = new Table;
        $table->name = 'sizes';
        $table->save();        

        $field = new Field;
        $field->name = 'title';

        $table->fields()->save($field);

        /**/  

        /****************************
        *   tags seeder  
        */

        $table = new Table;
        $table->name = 'tags';
        $table->save();        

        $field = new Field;
        $field->name = 'title';

        $table->fields()->save($field);

        /**/  

        /****************************
        *   facts seeder  
        */

        $table = new Table;
        $table->name = 'facts';
        $table->save();        

        $field = new Field;
        $field->name = 'title';

        $table->fields()->save($field);

        /**/

        /****************************
         *   units seeder
         */

        $table = new Table;
        $table->name = 'units';
        $table->save();

        $field = new Field;
        $field->name = 'title';

        $table->fields()->save($field);

        /**/

        /****************************
         *   pages seeder
         */

        $table = new Table;
        $table->name = 'pages';
        $table->save();

        $field = new Field;
        $field->name = 'title';

        $table->fields()->save($field);

        /**/

    }
}
