<?php

use Illuminate\Database\Seeder;

class LanguagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('languages')->delete();
        DB::table('languages')->insert(array(
            array('is_active' => 1, 'name' => 'farsi', 'native_name'=>'فارسی', 'abbr'=>'fa', 'dir' => 1),
            array('is_active' => 1, 'name' => 'english', 'native_name'=>'English', 'abbr'=>'en', 'dir' => 0),
        ));
    }
}
