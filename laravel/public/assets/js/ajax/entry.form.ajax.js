var entry_form_errors = [];
$('#entry-form').submit(function (event) {
    event.preventDefault();
    var $this = $(this);
    var url = $this.attr('action');
    var formData = new FormData($this[0]);
    $.ajax({
        url: url,
        type: 'POST',
        data: formData,
        dataType: 'JSON',
        processData: false,
        contentType: false,
        success: function (data) {
            window.location = data.redirect_url;
        },
        error: function (data) {
            if (data.status === 422) {
                var obj_name;
                var errors = data.responseJSON;
                $.each(entry_form_errors, function (index, field) {
                    obj_name = $('#' + field);
                    obj_type = obj_name.attr('type');
                    if (obj_type == 'select-one')
                        obj_name = $('#select2-' + field + '-container');
                    // else if (obj_type == 'select-multiple')
                    obj_name.removeClass('error');
                });
                entry_form_errors = [];
                $.each(errors, function (fields, msg_text) {
                    obj_name = $('#' + fields);
                    obj_type = obj_name.prop('type')
                    if (obj_type == 'select-one')
                        obj_name = $('#select2-' + fields + '-container').parent();
                    obj_name.addClass('error');
                    show_tooltip($this, obj_name, msg_text);
                    entry_form_errors.push(fields);
                });
            } else
                alert(data.statusText);
        }
    });
});

function show_tooltip(msg_button, msg_target, msg_text) {
    new jBox('Tooltip', {
        attach: msg_button,
        target: msg_target,
        // audio: '/assets/plugins/jbox/audio/bling',
        content: msg_text + '',
        theme: 'TooltipBorder',
        trigger: 'click',
        closeButton: 'box',
        animation: 'flip',
        position: {
            x: 'left',
            y: 'top'
        },
        outside: 'y',
        pointer: 'left:10',
        offset: {
            x: 25
        },
        onClose: function () {
            this.hide();
            this.destroy();
        }
    }).open();
}
