@extends("layouts.admin.master")

@section("page-name", trans("captions.file_manager"))

@section("content")
    <!-- page title -->
    <header id="page-header">
        <h1><i class="fa fa-file"></i> {{ trans("captions.file_manager") }}</h1>
    </header>
    <!-- /page title -->

    <div id="content" class="padding-0">
        <iframe frameborder="0" style="width: 100%; min-height: 600px;"
                src="/filemanager/dialog.php?type=0">
        </iframe>
    </div>
@endsection
