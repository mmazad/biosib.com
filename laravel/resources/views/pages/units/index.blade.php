@extends("layouts.admin.index-master")

@section("main-page", trans("captions.basic_info"))

@section("sub-page", trans("captions.units"))

@section("page-icon")<i class="fa fa-cogs"></i>@endsection

@section("page-index-href", route("admin.basics.units.index"))

@section("page-create-href", route("admin.basics.units.create"))

@section("table-thead")
    <th width="30">{{ trans("captions.status") }}</th>
    <th>{{ trans("captions.name") }}</th>
    <th>{{ trans("captions.food_title") }}</th>
    <th width="45"></th>
@endsection

@section("table-tbody")
    @foreach($units as $unit)
        <tr>
            <td class="text-center text-active">
                @if($unit->is_active)
                    <i class="mdi mdi-check text-green"></i>
                @else
                    <i class="mdi mdi-window-close text-red"></i>
                @endif
            </td>
            <td>{{ $unit->name }}</td>
            <td>{{ $unit->field('title')->translation }}</td>
            <td class="text-center">
                <a href="{{ route("admin.basics.units.edit", $unit->id) }}" class="btn btn-xs btn-warning"
                   title="{{ trans("captions.edit") }}"><i class="fa fa-pencil"></i></a>
                <a data-confirm="{{ trans("messages.info.delete") }}" onclick="delete_record('{{ route('admin.basics.units.delete', $unit->id)}}')"
                   class="btn btn-xs btn-red"
                   title="{{ trans("captions.delete") }}"><i class="fa fa-trash"></i></a>
            </td>
        </tr>
    @endforeach
@endsection
