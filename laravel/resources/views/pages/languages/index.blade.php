@extends("layouts.admin.index-master")

@section("main-page", trans("captions.basic_info"))

@section("sub-page", trans("captions.languages"))

@section("page-icon")<i class="fa fa-cogs"></i>@endsection

@section("page-index-href", route("admin.basics.languages.index"))

@section("page-create-href", route("admin.basics.languages.create"))

@section("table-thead")
    <th width="30">{{ trans("captions.status") }}</th>
    <th>{{ trans("captions.name") }}</th>
    <th>{{ trans("captions.native_name") }}</th>
    <th>{{ trans("captions.abbr") }}</th>
    <th>{{ trans("captions.dir") }}</th>
    <th width="45"></th>
@endsection

@section("table-tbody")
    @foreach($languages as $language)
        <tr>
            <td class="text-center text-active">
                @if($language->is_active)
                    <i class="mdi mdi-check text-green"></i>
                @else
                    <i class="mdi mdi-window-close text-red"></i>
                @endif
            </td>
            <td>{{ $language->name }}</td>
            <td>{{ $language->native_name }}</td>
            <td>{{ $language->abbr }}</td>
            <td>
                @if($language->dir)
                    {{ trans("captions.rtl") }}
                @else
                    {{ trans("captions.ltr") }}
                @endif
            </td>
            <td class="text-center">
                <a href="{{ route("admin.basics.languages.edit", $language->id) }}" class="btn btn-xs btn-warning"
                   title="{{ trans("captions.edit") }}"><i class="fa fa-pencil"></i></a>
                <a data-confirm="{{ trans("messages.info.delete") }}" onclick="delete_record('{{ route('admin.basics.languages.delete', $language->id)}}')"
                   class="btn btn-xs btn-red"
                   title="{{ trans("captions.delete") }}"><i class="fa fa-trash"></i></a>
            </td>
        </tr>
    @endforeach

@endsection
