@extends("layouts.admin.form-master")

@section("main-page", trans("captions.basic_info"))

@section("sub-page", trans("captions.languages"))

@section("page-name", trans("captions.create-info"))

@section("page-icon")<i class="fa fa-cogs"></i>@endsection

@section("page-index-href", route("admin.basics.languages.index"))

@section("form-content")
    {{ Form::open(['route' => 'admin.basics.languages.store', "id" => "entry-form"]) }}
    <fieldset>
        <div class="row">
            <div class="col-md-6 col-sm-6">
                <!-- required [php action request] -->
                <input type="hidden" name="action" value="store"/>

                <div class="row">
                    <div class="form-group">
                        <div class="col-sm-12">
                            <label class="checkbox">
                                {{ Form::checkbox("is_active", null) }}
                                <i></i> {{ trans("captions.is_active") }}
                            </label>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="form-group">
                        <div class="col-sm-12">
                            {{ Form::label('dir', trans("captions.dir")) }}
                            {{ Form::select("dir", ["0" => trans('captions.ltr'), "1" => trans('captions.rtl')], null, ["class" => "form-control"]) }}
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="form-group">
                        <div class="col-sm-12">
                            {{ Form::label('name', trans("captions.name") . " *") }}
                            <small class="text-muted">{{ trans("captions.regex_name") }}</small>
                            {{ Form::text("name", null, ["maxlength" => "50", "class" => "form-control", "placeholder" => trans('captions.name')]) }}
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="form-group">
                        <div class="col-sm-12">
                            {{ Form::label('abbr', trans("captions.abbr") . " *") }}
                            {{ Form::text("abbr", null, ["maxlength" => "4", "class" => "form-control", "placeholder" => trans('captions.abbr')]) }}
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="form-group">
                        <div class="col-sm-12">
                            {{ Form::label('native_name', trans("captions.native_name") . " *") }}
                            {{ Form::text("native_name", null, ["maxlength" => "50", "class" => "form-control", "placeholder" => trans('captions.native_name')]) }}
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="form-group">
                        <div class="col-sm-12">
                            <button type="submit"
                                    class="btn btn-3d btn-xlg btn-block btn-reveal btn-red">
                                <i class="fa fa-check"></i>
                                <span>{{ trans("captions.store") }}</span>
                            </button>
                        </div>
                    </div>
                </div>

            </div>
            <div class="col-md-6 col-sm-6">

            </div>
        </div>
    </fieldset>
    {{ Form::close() }}
@endsection
