@extends("layouts.admin.index-master")

@section("main-page", trans("captions.foods"))

@section("sub-page", trans("captions.food_info"))

@section("page-icon")<i class="fa fa-cutlery"></i>@endsection

@section("page-show-href", route("admin.foods.index"))

@section("page-create-href", route("admin.foods.create"))

@section("table-thead")
    <th width="30">{{ trans("captions.status") }}</th>
    <th>{{ trans("captions.food_title") }}</th>
    <th>{{ trans("captions.food_group") }}</th>
    <th width="40">{{ trans("captions.edit") }}</th>
    <th width="40">{{ trans("captions.delete") }}</th>
@endsection

@section("table-tbody")

@endsection