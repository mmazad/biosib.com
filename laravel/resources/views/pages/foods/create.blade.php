@extends("layouts.admin.form-master")

@section("main-page", trans("captions.foods"))

@section("sub-page", trans("captions.food_info"))

@section("page-name", trans("captions.create-info"))

@section("page-icon")<i class="fa fa-cutlery"></i>@endsection

@section("page-index-href", route("admin.foods.index"))

@section("page-create-href", route("admin.foods.create"))

@section("form-content")
    {{ Form::open(['route' => 'admin.foods.store', "id" => "entry-form"]) }}
    <fieldset>
        <div class="row">

            <div class="col-sm-12 margin-bottom-10">
                <label class="checkbox">
                    {{ Form::checkbox("is_active", null) }}
                    <i></i> {{ trans("captions.is_active") }}
                </label>
            </div>

            <div class="col-md-6 col-sm-6">
                <!-- required [php action request] -->
                <input type="hidden" name="action" value="store"/>

                <div class="row">
                    <div class="form-group">
                        <div class="col-sm-12">
                            {{ Form::label('food_group_id', trans("captions.food_group") . " *") }}
                            {{ Form::select("food_group_id", $food_group_ids, null, ["class" => "form-control pointer required select2"]) }}
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="form-group">
                        <div class="col-sm-12">
                            {{ Form::label('name', trans("captions.name") . " *") }}
                            <small class="text-muted">{{ trans("captions.regex_name") }}</small>
                            {{ Form::text("name", null, ["maxlength" => "100", "class" => "form-control", "placeholder" => trans('captions.name')]) }}
                        </div>
                    </div>
                </div>

                @foreach($languages as $language)
                    <div class="row">
                        <div class="form-group">
                            <div class="col-sm-12">
                                {{ Form::label('title-' . $language->id, trans("captions.food_title").'&nbsp;'.$language->native_name . " *") }}
                                {{ Form::text("title-" . $language->id, null, ["maxlength" => "100", "class" => "form-control", "placeholder" => trans('captions.food_title').'&nbsp;'.$language->native_name]) }}
                            </div>
                        </div>
                    </div>
                @endforeach

                <div class="row">
                    <div class="form-group">
                        <div class="col-sm-12">
                            {{ Form::label('tag_id', trans("captions.tags")) }}
                            {{ Form::select("tag_id", $tag_ids, null, ["multiple", "class" => "form-control pointer required select2"]) }}
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="form-group">
                        <div class="col-sm-12">
                            {{ Form::label('page_id', trans("captions.pages")) }}
                            {{ Form::select("page_id", $page_ids, null, ["multiple", "class" => "form-control pointer required select2"]) }}
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="form-group">
                        <div class="col-sm-12">
                            {{ Form::label('size_id', trans("captions.sizes")) }}
                            {{ Form::select("size_id", $size_ids, null, ["multiple", "class" => "form-control pointer required select2"]) }}
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="form-group">
                        <div class="col-sm-12">
                            <button type="submit"
                                    class="btn btn-3d btn-xlg btn-block btn-reveal btn-red">
                                <i class="fa fa-check"></i>
                                <span>{{ trans("captions.store") }}</span>
                            </button>
                        </div>
                    </div>
                </div>

            </div>
            <div class="col-md-6 col-sm-6">
                <div class="row">
                    <div class="form-group">
                        <div class="col-sm-12">
                            {{ Form::label('images', trans("captions.images")) }}
                            <div class="fancy-file-upload fancy-file-primary">
                                <i class="fa fa-image"></i>
                                {{ Form::text("select_image", null, ["onchange" => "add_img_list()", "class" => "form-control", "id" => "img-path", "placeholder" => trans("captions.image_title"), "readonly"]) }}
                                <span class="button pointer" data-toggle="modal"
                                      data-target=".bs-example-modal-lg">{{ trans("captions.select") }}</span>
                            </div>
                            <textarea name="food_images" id="nestable-list-output"
                                      class="hidden"></textarea>
                            <div id="nestable-list">
                                <ol class="dd-list"></ol>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </fieldset>
    {{ Form::close() }}

    <!-- modal -->
    <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">

                <!-- header modal -->
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myLargeModalLabel"><i
                                class="fa fa-image"></i> {{ trans("captions.image_select") }}</h4>
                </div>

                <!-- body modal -->
                <div class="modal-body">
                    <iframe frameborder="0" style="width: 100%; min-height: 600px;"
                            src="/filemanager/dialog.php?type=1&field_id=img-path&relative_url=1">
                    </iframe>
                </div>

            </div>
        </div>
    </div>
@endsection

@section("form-stylesheet")
    <link href="{{ asset("assets/css/layout-nestable.css") }}" rel="stylesheet" type="text/css"/>
@endsection

@section("form-script")
    <script type="text/javascript" src="{{ asset("assets/plugins/nestable/jquery.nestable.js") }}"></script>
    <script type="text/javascript" src="{{ asset("assets/js/tinymce.min.js") }}"></script>
    <script type="text/javascript">
        tinymce.init({
            selector: ".tiny-textarea",theme: "modern",width: 680,height: 300,
            plugins: [
                "advlist autolink link image lists charmap print preview hr anchor pagebreak",
                "searchreplace wordcount visualblocks visualchars insertdatetime media nonbreaking",
                "table contextmenu directionality emoticons paste textcolor responsivefilemanager code"
            ],
            toolbar1: "undo redo | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | styleselect",
            toolbar2: "| responsivefilemanager | link unlink anchor | image media | forecolor backcolor  | print preview code ",
            image_advtab: true ,

            external_filemanager_path:"/filemanager/",
            filemanager_title:"Responsive Filemanager" ,
            external_plugins: { "filemanager" : "/filemanager/plugin.min.js"}
        });
    </script>
    <script type="text/javascript">
        if (jQuery().nestable) {
            maxLevels: 1;
            var updateOutput = function (e) {
                var list = e.length ? e : $(e.target),
                        output = list.data('output');
                if (window.JSON) {
                    output.val(window.JSON.stringify(list.nestable('serialize'))); //, null, 2));
                } else {
                    output.val('JSON browser support required for this demo.');
                }
            };

            // Nestable list 1
            jQuery('#nestable-list').nestable({
                group: 1,
                maxDepth: 2
            }).on('change', updateOutput);

            // output initial serialised data
            updateOutput(jQuery('#nestable-list').data('output', jQuery('#nestable-list-output')));
        }

        var img_list_food = [];
        function add_img_list() {
            img_list_food = [];
            var mylist = JSON.parse($("#nestable-list-output").val());
            for (var i = 0; i < mylist.length; i++) {
                img_list_food.push(mylist[i].id);
                if (mylist[i].children) {
                    for (var j = 0; j < mylist[i].children.length; j++)
                        img_list_food.push(mylist[i].children[j].id);
                }
            }
            var img_path = $("#img-path").val();
            if (img_list_food.indexOf(img_path) == -1) {
                $("#nestable-list > .dd-list").append(
                        "<li class='dd-item' data-id='" + img_path + "'>" +
                        "<a onclick='remove_img_list($(this))' title='" + img_path + "'><i class='fa fa-times-circle'></i></a>" +
                        "<div class='dd-handle'>" +
                        "<img src='/assets/files/" + img_path + "' alt='' height='30'>" +
                        "&nbsp;<span>" + img_path + "</span>" +
                        "</div>" +
                        "</li>");
                $("#nestable-list").change();
            }
        }

        function remove_img_list(item) {
            item.parent().remove();
            $("#nestable-list").change();
        }
    </script>
@endsection