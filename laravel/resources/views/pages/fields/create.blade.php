@extends("layouts.admin.form-master")

@section("main-page", trans("captions.basic_info"))

@section("sub-page", trans("captions.fields"))

@section("page-name", trans("captions.create-info"))

@section("page-icon")<i class="fa fa-cogs"></i>@endsection

@section("page-index-href", route("admin.basics.fields.index"))

@section("page-create-href", route("admin.foods.create"))

@section("form-content")
    {{ Form::open(['route' => 'admin.basics.fields.store', "id" => "entry-form"]) }}
    <fieldset>
        <div class="row">
            <div class="col-md-6 col-sm-6">
                <!-- required [php action request] -->
                <input type="hidden" name="action" value="store"/>

                <div class="row">
                    <div class="form-group">
                        <div class="col-sm-12">
                            {{ Form::label('table_id', trans("captions.table_title") . " *") }}
                            {{ Form::select("table_id", $table_ids, null, ["class" => "form-control pointer required select2"]) }}
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="form-group">
                        <div class="col-sm-12">
                            {{ Form::label('name', trans("captions.name") . " *") }}
                            <small class="text-muted">{{ trans("captions.field_title") . " ". trans("captions.regex_name") }}</small>
                            {{ Form::text("name", null, ["maxlength" => "50", "class" => "form-control", "placeholder" => trans('captions.name')]) }}
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="form-group">
                        <div class="col-sm-12">
                            <button type="submit"
                                    class="btn btn-3d btn-xlg btn-block btn-reveal btn-red">
                                <i class="fa fa-check"></i>
                                <span>{{ trans("captions.store") }}</span>
                            </button>
                        </div>
                    </div>
                </div>

            </div>
            <div class="col-md-6 col-sm-6">

            </div>
        </div>
    </fieldset>
    {{ Form::close() }}
@endsection
