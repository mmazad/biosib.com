@extends("layouts.admin.form-master")

@section("main-page", trans("captions.foods"))

@section("sub-page", trans("captions.facts"))

@section("page-name", trans("captions.create-info"))

@section("page-icon")<i class="fa fa-cutlery"></i>@endsection

@section("page-index-href", route("admin.foods.facts.index"))

@section("form-content")
    {{ Form::open(['route' => 'admin.foods.facts.store', "id" => "entry-form"]) }}
    <fieldset>
        <div class="row">
            <div class="col-md-6 col-sm-6">
                <!-- required [php action request] -->
                <input type="hidden" name="action" value="store"/>

                <div class="row">
                    <div class="form-group">
                        <div class="col-sm-12">
                            <label class="checkbox">
                                {{ Form::checkbox("is_active", null) }}
                                <i></i> {{ trans("captions.is_active") }}
                            </label>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="form-group">
                        <div class="col-sm-12">
                            {{ Form::label('unit_id', trans("captions.unit") . " *") }}
                            {{ Form::select("unit_id", $unit_ids, null, ["class" => "form-control pointer required select2"]) }}
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="form-group">
                        <div class="col-sm-12">
                            {{ Form::label('name', trans("captions.name") . " *") }}
                            <small class="text-muted">{{ trans("captions.regex_name") }}</small>
                            {{ Form::text("name", null, ["maxlength" => "100", "class" => "form-control", "placeholder" => trans('captions.name')]) }}
                        </div>
                    </div>
                </div>
                @foreach($languages as $language)
                    <div class="row">
                        <div class="form-group">
                            <div class="col-sm-12">
                                {{ Form::label('title-' . $language->id, trans("captions.food_title").'&nbsp;'.$language->native_name . " *") }}
                                {{ Form::text("title-" . $language->id, null, ["maxlength" => "100", "class" => "form-control", "placeholder" => trans('captions.food_title').'&nbsp;'.$language->native_name]) }}
                            </div>
                        </div>
                    </div>
                @endforeach
                <div class="row">
                    <div class="form-group">
                        <div class="col-sm-12">
                            <button type="submit"
                                    class="btn btn-3d btn-xlg btn-block btn-reveal btn-red">
                                <i class="fa fa-check"></i>
                                <span>{{ trans("captions.store") }}</span>
                            </button>
                        </div>
                    </div>
                </div>

            </div>
            <div class="col-md-6 col-sm-6">

            </div>
        </div>
    </fieldset>
    {{ Form::close() }}
@endsection

