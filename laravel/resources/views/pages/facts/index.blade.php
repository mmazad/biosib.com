@extends("layouts.admin.index-master")

@section("main-page", trans("captions.foods"))

@section("sub-page", trans("captions.facts"))

@section("page-icon")<i class="fa fa-cutlery"></i>@endsection

@section("page-index-href", route("admin.foods.facts.index"))

@section("page-create-href", route("admin.foods.facts.create"))

@section("table-thead")
    <th width="30">{{ trans("captions.status") }}</th>
    <th>{{ trans("captions.name") }}</th>
    <th>{{ trans("captions.food_title") }}</th>
    <th width="45"></th>
@endsection

@section("table-tbody")
    @foreach($facts as $fact)
        <tr>
            <td class="text-center text-active">
                @if($fact->is_active)
                    <i class="mdi mdi-check text-green"></i>
                @else
                    <i class="mdi mdi-window-close text-red"></i>
                @endif
            </td>
            <td>{{ $fact->name }}</td>
            <td>{{ $fact->field('title')->translation }}</td>
            <td class="text-center">
                <a href="{{ route("admin.foods.facts.edit", $fact->id) }}" class="btn btn-xs btn-warning"
                   title="{{ trans("captions.edit") }}"><i class="fa fa-pencil"></i></a>
                <a data-confirm="{{ trans("messages.info.delete") }}" onclick="delete_record('{{ route('admin.foods.facts.delete', $fact->id)}}')"
                   class="btn btn-xs btn-red"
                   title="{{ trans("captions.delete") }}"><i class="fa fa-trash"></i></a>
            </td>
        </tr>
    @endforeach

@endsection
