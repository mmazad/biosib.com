@extends("layouts.admin.form-master")

@section("main-page", trans("captions.basic_info"))

@section("sub-page", trans("captions.tables"))

@section("page-name", trans("captions.edit-info"))

@section("page-icon")<i class="fa fa-cogs"></i>@endsection

@section("page-index-href", route("admin.basics.tables.index"))

@section("form-content")
    {{ Form::model($table, ['route' => ['admin.basics.tables.update', $table->id], "id" => "entry-form"]) }}
    <fieldset>
        <div class="row">
            <div class="col-md-6 col-sm-6">
                <!-- required [php action request] -->
                <input type="hidden" name="action" value="update"/>

                <div class="row">
                    <div class="form-group">
                        <div class="col-sm-12">
                            {{ Form::label('name', trans("captions.name") . " *") }}
                            <small class="text-muted">{{ trans("captions.regex_name") }}</small>
                            {{ Form::text("name", null, ["maxlength" => "50", "class" => "form-control", "placeholder" => trans('captions.name')]) }}
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="form-group">
                        <div class="col-sm-12">
                            <button type="submit"
                                    class="btn btn-3d btn-xlg btn-block btn-reveal btn-red">
                                <i class="fa fa-check"></i>
                                <span>{{ trans("captions.store") }}</span>
                            </button>
                        </div>
                    </div>
                </div>

            </div>
            <div class="col-md-6 col-sm-6">

            </div>
        </div>
    </fieldset>
    {{ Form::close() }}
@endsection
