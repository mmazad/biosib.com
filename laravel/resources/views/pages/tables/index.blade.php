@extends("layouts.admin.index-master")

@section("main-page", trans("captions.basic_info"))

@section("sub-page", trans("captions.tables"))

@section("page-icon")<i class="fa fa-cogs"></i>@endsection

@section("page-index-href", route("admin.basics.tables.index"))

@section("page-create-href", route("admin.basics.tables.create"))

@section("table-thead")
    <th>{{ trans("captions.name") }}</th>
    <th width="45"></th>
@endsection

@section("table-tbody")
    @foreach($tables as $table)
        <tr>
            <td>{{ $table->name }}</td>
            <td class="text-center">
                <a href="{{ route("admin.basics.tables.edit", $table->id) }}" class="btn btn-xs btn-warning"
                   title="{{ trans("captions.edit") }}"><i class="fa fa-pencil"></i></a>
                <a data-confirm="{{ trans("messages.info.delete") }}" onclick="delete_record('{{ route('admin.basics.tables.delete', $table->id)}}')"
                   class="btn btn-xs btn-red"
                   title="{{ trans("captions.delete") }}"><i class="fa fa-trash"></i></a>
            </td>
        </tr>
    @endforeach

@endsection
