@extends("layouts.admin.index-master")

@section("main-page", trans("captions.basic_info"))

@section("sub-page", trans("captions.tags"))

@section("page-icon")<i class="fa fa-cogs"></i>@endsection

@section("page-index-href", route("admin.basics.tags.index"))

@section("page-create-href", route("admin.basics.tags.create"))

@section("table-thead")
    <th width="30">{{ trans("captions.status") }}</th>
    <th>{{ trans("captions.name") }}</th>
    <th>{{ trans("captions.food_title") }}</th>
    <th width="45"></th>
@endsection

@section("table-tbody")
    @foreach($tags as $tag)
        <tr>
            <td class="text-center text-active">
                @if($tag->is_active)
                    <i class="mdi mdi-check text-green"></i>
                @else
                    <i class="mdi mdi-window-close text-red"></i>
                @endif
            </td>
            <td>{{ $tag->name }}</td>
            <td>{{ $tag->field('title')->translation }}</td>
            <td class="text-center">
                <a href="{{ route("admin.basics.tags.edit", $tag->id) }}" class="btn btn-xs btn-warning"
                   title="{{ trans("captions.edit") }}"><i class="fa fa-pencil"></i></a>
                <a data-confirm="{{ trans("messages.info.delete") }}" onclick="delete_record('{{ route('admin.basics.tags.delete', $tag->id)}}')"
                   class="btn btn-xs btn-red"
                   title="{{ trans("captions.delete") }}"><i class="fa fa-trash"></i></a>
            </td>
        </tr>
    @endforeach

@endsection
