@extends("layouts.admin.master")

@section("content")
    <!-- page title -->
    <header id="page-header">
        <h1>@yield("page-icon") @yield("main-page")</h1>
        <ol class="breadcrumb">
            <li><a href="@yield("page-index-href")">@yield("sub-page")</a></li>
            <li class="active">@yield("page-name")</li>
        </ol>
    </header>
    <!-- /page title -->

    <div id="content" class="padding-10">
        <div class="row">
            <div class="col-md-12">
                <div id="panel-1" class="panel panel-default">
                    <div class="panel-heading">
                        <span class="title elipsis">
                            <strong>@yield("page-name")</strong> <!-- panel title -->
                        </span>
                        <!-- right options -->
                        <ul class="options pull-right list-inline">
                            <li>
                                <a class="opt panel_colapse" data-toggle="tooltip" data-placement="bottom"></a>
                            </li>
                            <li>
                                <a class="opt panel_fullscreen hidden-xs" data-toggle="tooltip"
                                   data-placement="bottom"><i class="fa fa-expand"></i></a>
                            </li>
                            <li>
                                <a href="@yield("page-index-href")" class="opt"><i class="fa fa-arrow-left"></i></a>
                            </li>
                        </ul>
                        <!-- /right options -->
                    </div>
                    <!-- panel content -->
                    <div class="panel-body">
                        @yield("form-content")
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section("stylesheet")
    <link href="{{ asset("assets/plugins/jbox/css/jBox.css") }}" rel="stylesheet" type="text/css">
    @yield("form-stylesheet")
@endsection

@section("script")
    <script type="text/javascript" src="{{ asset("assets/plugins/jbox/js/jBox.js") }}"></script>
    <script type="text/javascript" src="{{ asset("assets/js/ajax/entry.form.ajax.js") }}"></script>
    @yield("form-script")
@endsection
