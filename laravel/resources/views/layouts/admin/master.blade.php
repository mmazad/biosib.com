<!doctype html>
<html lang="en-US">
<head>
    <meta charset="utf-8"/>
    <title>{{ trans("captions.site_name") }} - @yield("sub-page")</title>
    <meta name="fontiran.com:license" content="LKUFM">
    <meta http-equiv="Content-type" content="text/html; charset=utf-8"/>
    <meta name="Author" content="{{ trans("captions.site_author") }}"/>
    <meta name="description" content="{{ trans("captions.site_description") }}"/>

    <!-- mobile settings -->
    <meta name="viewport" content="width=device-width, maximum-scale=1, initial-scale=1, user-scalable=0"/>

    <!-- THEME CSS -->
    <link href="{{ asset("assets/plugins/mdi/css/materialdesignicons.css") }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset("assets/css/fontiran.css") }}" rel="stylesheet" type="text/css" id="color_scheme"/>
    <link href="{{ asset("assets/plugins/bootstrap/css/bootstrap.min.css") }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset("assets/css/essentials.css") }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset("assets/css/layout.css") }}" rel="stylesheet" type="text/css"/>
    @yield("stylesheet")

    <!-- CORE CSS -->
    <link href="{{ asset("assets/plugins/bootstrap/RTL/bootstrap-rtl.min.css") }}" rel="stylesheet" type="text/css">
    <link href="{{ asset("assets/plugins/bootstrap/RTL/bootstrap-flipped.min.css") }}" rel="stylesheet" type="text/css">
    <link href="{{ asset("assets/css/layout-RTL.css") }}" rel="stylesheet" type="text/css" id="rtl_ltr">

</head>
<!--
    .boxed = boxed version
-->
<body>
<div id="wrapper" class="clearfix">
    <aside id="aside">

        @include("partials.admin.navbar")
        <span id="asidebg"></span>

    </aside>

    @include("partials.admin.header")

    <section id="middle">
        @yield("content")
    </section>

</div>
<script type="text/javascript">var plugin_path = '/assets/plugins/';</script>
<script type="text/javascript" src="{{ asset("assets/plugins/jquery/jquery.js") }}"></script>
<script type="text/javascript" src="{{ asset("assets/js/app.js") }}"></script>
@yield("script")
</body>
</html>