@extends("layouts.admin.master")

@section("content")
    <!-- page title -->
    <header id="page-header">
        <h1>@yield("page-icon") @yield("main-page")</h1>
        <ol class="breadcrumb">
            <li><a href="@yield("page-index-href")">@yield("sub-page")</a></li>
            <li class="active">{{ trans("captions.show-info") }}</li>
        </ol>
    </header>
    <!-- /page title -->

    <div id="content" class="padding-10">
        <div class="row">
            <div class="col-md-12">
                <div id="panel-1" class="panel panel-default">
                    <div class="panel-heading">
                        <span class="title elipsis">
                            <strong>{{ trans("captions.show-info") }}</strong> <!-- panel title -->
                        </span>
                        <!-- right options -->
                        <ul class="options pull-right list-inline">
                            <li>
                                <a class="opt panel_colapse" data-toggle="tooltip" data-placement="bottom"></a>
                            </li>
                            <li>
                                <a class="opt panel_fullscreen hidden-xs" data-toggle="tooltip"
                                   data-placement="bottom"><i class="fa fa-expand"></i></a>
                            </li>
                            <li>
                                <a href="@yield("page-create-href")" class="opt"><i class="fa fa-plus"></i></a>
                            </li>
                        </ul>
                        <!-- /right options -->
                    </div>
                    <!-- panel content -->
                    <div class="panel-body">
                        <table class="table table-striped table-bordered table-hover" id="grid-view">
                            <thead>
                            <tr>
                                @yield("table-thead")
                            </tr>
                            </thead>
                            <tbody>
                                @yield("table-tbody")
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section("stylesheet")
    <!-- CORE CSS -->
    <link href="{{ asset("assets/plugins/datatables/css/dataTables.bootstrap.css") }}" rel="stylesheet"
          type="text/css"/>
    <link href="{{ asset("assets/plugins/jbox/css/jBox.css") }}" rel="stylesheet" type="text/css">
    <link href="{{ asset("assets/plugins/jbox/css/jBox.Confirm.css") }}" rel="stylesheet" type="text/css">
    @yield("form-stylesheet")
@endsection

@section("script")
    <!-- JAVASCRIPT FILES -->
    <script type="text/javascript" src="{{ asset("assets/plugins/datatables/js/jquery.dataTables.js")}}"></script>
    <script type="text/javascript" src="{{ asset("assets/plugins/datatables/js/dataTables.bootstrap.js")}}"></script>
    <script type="text/javascript" src="{{ asset("assets/plugins/datatables/js/dataTables.buttons.js")}}"></script>
    <script type="text/javascript" src="{{ asset("assets/plugins/datatables/js/buttons.flash.js")}}"></script>
    <script type="text/javascript" src="{{ asset("assets/plugins/datatables/js/jszip.js")}}"></script>
    <script type="text/javascript" src="{{ asset("assets/plugins/datatables/js/buttons.html5.js")}}"></script>
    <script type="text/javascript" src="{{ asset("assets/plugins/datatables/js/buttons.print.js")}}"></script>
    <script type="text/javascript" src="{{ asset("assets/plugins/datatables/js/buttons.colVis.js")}}"></script>
    <script type="text/javascript" src="{{ asset("assets/plugins/datatables/js/buttons.bootstrap.js")}}"></script>

    <script type="text/javascript">

        var extensions = {
            "sFilterInput": "form-control",
            "sLengthSelect": "form-control"
        }
        $.extend($.fn.dataTableExt.oStdClasses, extensions);

        $('#grid-view').DataTable({
            stateSave: true,
//            "processing": true,
//            "serverSide": true,
//            "ajax": $.fn.dataTable.pipeline( {
//                url: 'scripts/server_processing.php',
//                pages: 5 // number of pages to cache
//            } ),
            "dom": "<'row' <'col-md-12'T>><'row'<'col-md-4 col-sm-12'l><'col-md-4 col-sm-12'f><'col-md-4 col-sm-12 text-right'B>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>",
            buttons: [
                {
                    extend: 'print',
                    text: '<i class="mdi mdi-printer"></i>',
                    exportOptions: {
                        columns: [':visible:not(.action)']
                    }
                },
                {
                    extend: 'excel',
                    text: '<i class="mdi mdi-file-excel-box"></i>',
                    exportOptions: {
                        columns: [':visible:not(.action)']
                    }
                },
                {
                    extend: 'colvis',
                    text: '<i class="mdi mdi-table-edit"></i>',
                    columns: [':not(.action)']
                },
            ]
        });

        //
        // Pipelining function for DataTables. To be used to the `ajax` option of DataTables
        //
        $.fn.dataTable.pipeline = function (opts) {
            // Configuration options
            var conf = $.extend({
                pages: 5,     // number of pages to cache
                url: '',      // script url
                data: null,   // function or object with parameters to send to the server
                              // matching how `ajax.data` works in DataTables
                method: 'GET' // Ajax HTTP method
            }, opts);

            // Private variables for storing the cache
            var cacheLower = -1;
            var cacheUpper = null;
            var cacheLastRequest = null;
            var cacheLastJson = null;

            return function (request, drawCallback, settings) {
                var ajax = false;
                var requestStart = request.start;
                var drawStart = request.start;
                var requestLength = request.length;
                var requestEnd = requestStart + requestLength;

                if (settings.clearCache) {
                    // API requested that the cache be cleared
                    ajax = true;
                    settings.clearCache = false;
                }
                else if (cacheLower < 0 || requestStart < cacheLower || requestEnd > cacheUpper) {
                    // outside cached data - need to make a request
                    ajax = true;
                }
                else if (JSON.stringify(request.order) !== JSON.stringify(cacheLastRequest.order) ||
                        JSON.stringify(request.columns) !== JSON.stringify(cacheLastRequest.columns) ||
                        JSON.stringify(request.search) !== JSON.stringify(cacheLastRequest.search)
                ) {
                    // properties changed (ordering, columns, searching)
                    ajax = true;
                }

                // Store the request for checking next time around
                cacheLastRequest = $.extend(true, {}, request);

                if (ajax) {
                    // Need data from the server
                    if (requestStart < cacheLower) {
                        requestStart = requestStart - (requestLength * (conf.pages - 1));

                        if (requestStart < 0) {
                            requestStart = 0;
                        }
                    }

                    cacheLower = requestStart;
                    cacheUpper = requestStart + (requestLength * conf.pages);

                    request.start = requestStart;
                    request.length = requestLength * conf.pages;

                    // Provide the same `data` options as DataTables.
                    if ($.isFunction(conf.data)) {
                        // As a function it is executed with the data object as an arg
                        // for manipulation. If an object is returned, it is used as the
                        // data object to submit
                        var d = conf.data(request);
                        if (d) {
                            $.extend(request, d);
                        }
                    }
                    else if ($.isPlainObject(conf.data)) {
                        // As an object, the data given extends the default
                        $.extend(request, conf.data);
                    }

                    settings.jqXHR = $.ajax({
                        "type": conf.method,
                        "url": conf.url,
                        "data": request,
                        "dataType": "json",
                        "cache": false,
                        "success": function (json) {
                            cacheLastJson = $.extend(true, {}, json);

                            if (cacheLower != drawStart) {
                                json.data.splice(0, drawStart - cacheLower);
                            }
                            if (requestLength >= -1) {
                                json.data.splice(requestLength, json.data.length);
                            }

                            drawCallback(json);
                        }
                    });
                }
                else {
                    json = $.extend(true, {}, cacheLastJson);
                    json.draw = request.draw; // Update the echo for each response
                    json.data.splice(0, requestStart - cacheLower);
                    json.data.splice(requestLength, json.data.length);

                    drawCallback(json);
                }
            }
        };

        // Register an API method that will empty the pipelined data, forcing an Ajax
        // fetch on the next draw (i.e. `table.clearPipeline().draw()`)
        $.fn.dataTable.Api.register('clearPipeline()', function () {
            return this.iterator('table', function (settings) {
                settings.clearCache = true;
            });
        });
    </script>

    <script type="text/javascript" src="{{ asset("assets/plugins/jbox/js/jBox.js") }}"></script>
    <script type="text/javascript" src="{{ asset("assets/plugins/jbox/js/jBox.Confirm.js") }}"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            new jBox('Confirm', {
                cancelButton: 'خیر',
                confirmButton: 'بله',
            });
        });

        function delete_record(route) {
            window.location = route;
        }
    </script>
    @yield("form-script")
@endsection
