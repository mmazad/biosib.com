<?php
$main_page = $sub_page = "not found";
$url = explode('/', $_SERVER['REQUEST_URI']);
if (count($url) >= 3)
    $main_page = $url[2];
if (count($url) >= 4)
    $sub_page = $url[3];
?>
<nav id="sideNav"><!-- MAIN MENU -->
    <ul class="nav nav-list">
        <li class="<?php if ($main_page == 'dashboard') echo 'active'; ?>"><!-- dashboard -->
            <a class="dashboard" href="{{ route("admin.dashboard") }}">
                <i class="main-icon fa fa-dashboard"></i> <span>{{ trans("captions.dashboard") }}</span>
            </a>
        </li>

        <li class="<?php if ($main_page == 'foods') echo 'active'; ?>"><!-- foods -->
            <a>
                <i class="fa fa-menu-arrow pull-right"></i>
                <i class="main-icon fa fa-cutlery"></i> <span>{{ trans("captions.foods") }}</span>
            </a>
            <ul><!-- submenus -->
                <li class="<?php if ($sub_page == 'groups') echo 'active'; ?>"><a
                            href="{{ route("admin.foods.groups.index") }}">{{ trans("captions.groups") }}</a></li>
                <li class="<?php if ($sub_page == 'sizes') echo 'active'; ?>"><a
                            href="{{ route("admin.foods.sizes.index") }}">{{ trans("captions.sizes") }}</a></li>
                <li class="<?php if ($sub_page == 'facts') echo 'active'; ?>"><a
                            href="{{ route("admin.foods.facts.index") }}">{{ trans("captions.facts") }}</a></li>
                <li class="<?php if ($sub_page == 'index') echo 'active'; ?>"><a
                            href="{{ route("admin.foods.index") }}">{{ trans("captions.food_info") }}</a></li>
            </ul>
        </li>

        <li class="<?php if ($main_page == 'pages') echo 'active'; ?>"><!-- pages -->
            <a href="{{ route("admin.pages.index") }}">
                <i class="main-icon fa fa-globe"></i> <span>{{ trans("captions.pages") }}</span>
            </a>
        </li>

        <li class="<?php if ($main_page == 'activities') echo 'active'; ?>"><!-- activities -->
            <a href="{{ route("admin.activities.index") }}">
                <i class="main-icon fa fa-child"></i> <span>{{ trans("captions.activities") }}</span>
            </a>
        </li>

        <li class="<?php if ($main_page == 'users') echo 'active'; ?>"><!-- users -->
            <a>
                <i class="fa fa-menu-arrow pull-right"></i>
                <i class="main-icon fa fa-users"></i> <span>{{ trans("captions.users") }}</span>
            </a>
            <ul><!-- submenus -->
                <li class="<?php if ($sub_page == 'users') echo 'active'; ?>"><a
                            href="graphs-flot.html">{{ trans("captions.add_user") }}</a></li>
                <li class="<?php if ($sub_page == 'users') echo 'active'; ?>"><a
                            href="graphs-chartjs.html">{{ trans("captions.show_users") }}</a></li>
            </ul>
        </li>

        <li class="<?php if ($main_page == 'basics') echo 'active'; ?>"><!-- basic -->
            <a>
                <i class="fa fa-menu-arrow pull-right"></i>
                <i class="main-icon fa fa-cogs"></i> <span>{{ trans("captions.basic_info") }}</span>
            </a>
            <ul><!-- submenus -->
                <li class="<?php if ($sub_page == 'languages') echo 'active'; ?>"><a
                            href="{{ route("admin.basics.languages.index") }}">{{ trans("captions.languages") }}</a>
                </li>
                <li class="<?php if ($sub_page == 'tables') echo 'active'; ?>"><a
                            href="{{ route("admin.basics.tables.index") }}">{{ trans("captions.tables") }}</a></li>
                <li class="<?php if ($sub_page == 'fields') echo 'active'; ?>"><a
                            href="{{ route("admin.basics.fields.index") }}">{{ trans("captions.fields") }}</a></li>
                <li class="<?php if ($sub_page == 'units') echo 'active'; ?>"><a
                            href="{{ route("admin.basics.units.index") }}">{{ trans("captions.units") }}</a></li>
                <li class="<?php if ($sub_page == 'tags') echo 'active'; ?>"><a
                            href="{{ route("admin.basics.tags.index") }}">{{ trans("captions.tags") }}</a></li>
            </ul>
        </li>

    </ul>

    <!-- SECOND MAIN LIST -->
    <h3>{{ trans("captions.more") }}</h3>
    <ul class="nav nav-list">
        <li class="<?php if ($main_page == 'filemanager') echo 'active'; ?>">
            <a href="{{ route("admin.filemanager") }}">
                <i class="main-icon fa fa-file"></i>
                <span>{{ trans("captions.file_manager") }}</span>
            </a>
        </li>
        <li class="<?php if ($main_page == 'index') echo 'active'; ?>">
            <a href="/">
                <i class="main-icon fa fa-link"></i>
                <span>{{ trans("captions.site_name") }}</span>
            </a>
        </li>
    </ul>

</nav>