<div id="panel-1" class="panel panel-default">
    <div class="panel-heading">
                <span class="title elipsis">
                    <strong>{{ trans("captions.search_in_month") }}</strong> <!-- panel title -->
                    <small class="size-12 weight-300 text-mutted hidden-xs">{{ trans("captions.year_name") }}</small>
                </span>

        <!-- right options -->
        <ul class="options pull-right list-inline">
            <li><a href="#" class="opt panel_colapse" data-toggle="tooltip"
                   data-placement="bottom"></a></li>
            <li><a href="#" class="opt panel_fullscreen hidden-xs" data-toggle="tooltip"
                   data-placement="bottom"><i class="fa fa-expand"></i></a></li>
        </ul>
        <!-- /right options -->
    </div>

    <!-- panel content -->
    <div class="panel-body">

        <div id="flot-sales" class="fullwidth height-250"></div>

    </div>
    <!-- /panel content -->

    <!-- panel footer -->
    <div class="panel-footer">

        <ul class="easypiecharts list-unstyled">
            <li class="clearfix">
                <span class="stat-number">۱۸/۱۲۵</span>
                <span class="stat-title">{{ trans("captions.new_users") }}</span>
                <span class="easyPieChart" data-percent="86" data-easing="easeOutBounce" data-barColor="#F8CB00"
                      data-trackColor="#dddddd" data-scaleColor="#dddddd" data-size="60" data-lineWidth="4">
                            <span class="percent"></span>
                        </span>
            </li>

            <li class="clearfix">
                <span class="stat-number">۶۰%</span>
                <span class="stat-title">{{ trans("captions.returning_users") }}</span>

                <span class="easyPieChart" data-percent="59.83" data-easing="easeOutBounce"
                      data-barColor="#0058AA" data-trackColor="#dddddd" data-scaleColor="#dddddd" data-size="60"
                      data-lineWidth="4">
                            <span class="percent"></span>
                        </span>
            </li>

            <li class="clearfix">
                <span class="stat-number">۱۲%</span>
                <span class="stat-title">{{ trans("captions.negative_search") }}</span>

                <span class="easyPieChart" data-percent="12" data-easing="easeOutBounce" data-barColor="#F86C6B"
                      data-trackColor="#dddddd" data-scaleColor="#dddddd" data-size="60" data-lineWidth="4">
										<span class="percent"></span>
									</span>
            </li>
            <li class="clearfix">
                <span class="stat-number">۹۷%</span>
                <span class="stat-title">{{ trans("captions.positive_search") }}</span>

                <span class="easyPieChart" data-percent="97" data-easing="easeOutBounce" data-barColor="#98AD4E"
                      data-trackColor="#dddddd" data-scaleColor="#dddddd" data-size="60" data-lineWidth="4">
										<span class="percent"></span>
									</span>
            </li>
        </ul>

    </div>
    <!-- /panel footer -->
</div>