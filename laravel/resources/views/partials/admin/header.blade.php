<header id="header">

    <!-- Mobile Button -->
    <button id="mobileMenuBtn"></button>

    <!-- Logo -->
    <span class="logo pull-left">
					<img src="/assets/img/logo_light.png" alt="admin panel" height="35" />
				</span>

    <form method="get" action="page-search.html" class="search pull-left hidden-xs">
        <input type="text" class="form-control" name="k" placeholder="{{ trans("captions.search") }}" />
    </form>

    <nav>

        <!-- OPTIONS LIST -->
        <ul class="nav pull-right">

            <!-- USER OPTIONS -->
            <li class="dropdown pull-left">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                    <img class="user-avatar" alt="" src="/assets/img/noavatar.png" height="34" />
                    <span class="user-name">
									<span class="hidden-xs">
										{{ trans("captions.user_admin") }} <i class="fa fa-angle-down"></i>
									</span>
								</span>
                </a>
                <ul class="dropdown-menu hold-on-click">
                    <li><!-- my inbox -->
                        <a href="#"><i class="fa fa-envelope"></i> {{ trans("captions.inbox") }}
                            <span class="pull-right label label-default">0</span>
                        </a>
                    </li>
                    <li><!-- settings -->
                        <a href="page-user-profile.html"><i class="fa fa-cogs"></i> {{ trans("captions.settings") }}</a>
                    </li>

                    <li class="divider"></li>

                    <li><!-- logout -->
                        <a href="page-login.html"><i class="fa fa-power-off"></i> {{ trans("captions.logout") }}</a>
                    </li>
                </ul>
            </li>
            <!-- /USER OPTIONS -->

        </ul>
        <!-- /OPTIONS LIST -->

    </nav>

</header>