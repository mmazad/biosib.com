<div class="row">

    <!-- Feedback Box -->
    <div class="col-md-3 col-sm-6">

        <!-- BOX -->
        <div class="box danger"><!-- default, danger, warning, info, success -->

            <div class="box-title"><!-- add .noborder class if box-body is removed -->
                <h4><a href="#">{{ trans("captions.all_foods") }} ۵۴۶۳</a></h4>
                <small class="block">{{ trans("captions.count_foods") }} ۱۴۸</small>
                <i class="fa fa-coffee"></i>
            </div>

            <div class="box-body text-center">
									<span class="sparkline"
                                          data-plugin-options='{"type":"bar","barColor":"#ffffff","height":"35px","width":"100%","zeroAxis":"false","barSpacing":"2"}'>
										331,265,456,411,367,319,402,312,300,312,283,384,372,269,402,319,416,355,416,371,423,259,361,312,269,402,327
									</span>
            </div>

        </div>
        <!-- /BOX -->

    </div>

    <!-- Profit Box -->
    <div class="col-md-3 col-sm-6">

        <!-- BOX -->
        <div class="box warning"><!-- default, danger, warning, info, success -->

            <div class="box-title"><!-- add .noborder class if box-body is removed -->
                <h4>{{ trans("captions.all_messages") }} ۲۵۴۷</h4>
                <small class="block">{{ trans("captions.today_messages") }} ۱۴</small>
                <i class="fa fa-comments"></i>
            </div>

            <div class="box-body text-center">
                        <span class="sparkline"
                              data-plugin-options='{"type":"bar","barColor":"#ffffff","height":"35px","width":"100%","zeroAxis":"false","barSpacing":"2"}'>
                        331,265,456,411,367,319,402,312,300,312,283,384,372,269,402,319,416,355,416,371,423,259,361,312,269,402,327
                        </span>
            </div>

        </div>
        <!-- /BOX -->

    </div>

    <!-- Orders Box -->
    <div class="col-md-3 col-sm-6">

        <!-- BOX -->
        <div class="box default"><!-- default, danger, warning, info, success -->

            <div class="box-title"><!-- add .noborder class if box-body is removed -->
                <h4>{{ trans("captions.all_search") }} ۵۸۹۴۴</h4>
                <small class="block">{{ trans("captions.today_search") }} ۳۶</small>
                <i class="fa fa-search"></i>
            </div>

            <div class="box-body text-center">
                        <span class="sparkline"
                              data-plugin-options='{"type":"bar","barColor":"#ffffff","height":"35px","width":"100%","zeroAxis":"false","barSpacing":"2"}'>
                            331,265,456,411,367,319,402,312,300,312,283,384,372,269,402,319,416,355,416,371,423,259,361,312,269,402,327
                        </span>
            </div>

        </div>
        <!-- /BOX -->

    </div>

    <!-- Online Box -->
    <div class="col-md-3 col-sm-6">

        <!-- BOX -->
        <div class="box success"><!-- default, danger, warning, info, success -->

            <div class="box-title"><!-- add .noborder class if box-body is removed -->
                <h4>{{ trans("captions.online_users") }} ۴۶</h4>
                <small class="block">{{ trans("captions.visitors_today") }} ۳۶۸</small>
                <i class="fa fa-globe"></i>
            </div>

            <div class="box-body text-center">
									<span class="sparkline"
                                          data-plugin-options='{"type":"bar","barColor":"#ffffff","height":"35px","width":"100%","zeroAxis":"false","barSpacing":"2"}'>
										331,265,456,411,367,319,402,312,300,312,283,384,372,269,402,319,416,355,416,371,423,259,361,312,269,402,327
									</span>
            </div>

        </div>
        <!-- /BOX -->

    </div>

</div>