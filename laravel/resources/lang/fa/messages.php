<?php

return [

    "info" => [
      "delete" => "<i class='fa fa-trash'></i> از حذف اطلاعات اطمینان دارید؟",
    ],

    "success" => [
        "create" => "ثبت اطلاعات موفقیت آمیز بود.",
    ],

    "error" => [
        "create" => "ثبت اطلاعات موفقیت آمیز نبود.",
    ]
];
